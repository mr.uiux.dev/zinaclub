<!DOCTYPE html>
<?php
$current_page ='Home';
$timestamp = date("YmdHis");
// $site_path = 'https://www.creamyw.com/dev/zinaclub/assets';
$site_path = 'http://localhost/zinaclub/assets';
// $site_url = 'https://www.creamyw.com/dev/zinaclub/views';
$site_url = 'http://localhost/zinaclub/views';
?>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>ZinaClub | <?php echo $current_page ?></title>
    <?php include '../../component/head.php';?>
</head>

<body>
    <?php include '../../component/navbar.php';?>
    <div class="body__wraper">
        <div class="header">
            <div class="header__slider">
                <div>
                    <div class="header__item">
                        <div class="container">
                            <div class="row">
                                <div class="col-xl-10 col-lg-11 mx-auto">
                                    <div class="row align-items-center">
                                        <div class="col-md-5">
                                            <div class="img" data-animation-in="fadeIn" data-delay-in="2"
                                                data-duration-in="2" data-animation-out="fadeOUt" data-delay-out="2"
                                                data-duration-out="2">
                                                <img src="<?php echo $site_path; ?>/images/slide.jpg?w=440&h=518&mode=crop&scale=both"
                                                    class="img-fluid w-100" alt="" />
                                            </div>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="data bg-third" data-animation-in="fadeIn" data-delay-in="4"
                                                data-duration-in="4" data-animation-out="fadeOUt" data-delay-out="4"
                                                data-duration-out="4">
                                                <div class="heading">
                                                    <h1>
                                                        Welcome To
                                                        <span class="text-primary">Zina Club</span>
                                                    </h1>
                                                </div>
                                                <div class="desc">
                                                    <p>
                                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                                                        eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                                                        enim ad minim veniam, quis nostrud exercitation ullamco laboris
                                                        nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                                                        in
                                                        reprehenderit in voluptate velit esse cillum dolore eu fugiat
                                                        nulla pariatur. Excepteur sint occaecat cupidatat non proident,
                                                        sunt in culpa qui officia deserunt mollit anim id est laborum.
                                                    </p>
                                                </div>
                                                <div class="link text-end">
                                                    <a href="" class="btn">Read More</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="header__item">
                        <div class="container">
                            <div class="row">
                                <div class="col-xl-10 col-lg-11 mx-auto">
                                    <div class="row align-items-center">
                                        <div class="col-md-5">
                                            <div class="img" data-animation-in="fadeIn" data-delay-in="2"
                                                data-duration-in="2" data-animation-out="fadeOUt" data-delay-out="2"
                                                data-duration-out="2">
                                                <img src="<?php echo $site_path; ?>/images/slide.jpg?w=440&h=518&mode=crop&scale=both"
                                                    class="img-fluid w-100" alt="" />
                                            </div>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="data bg-third" data-animation-in="fadeIn" data-delay-in="4"
                                                data-duration-in="4" data-animation-out="fadeOUt" data-delay-out="4"
                                                data-duration-out="4">
                                                <div class="heading">
                                                    <h1>
                                                        Welcome To
                                                        <span class="text-primary">Zina Club</span>
                                                    </h1>
                                                </div>
                                                <div class="desc">
                                                    <p>
                                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                                                        eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                                                        enim ad minim veniam, quis nostrud exercitation ullamco laboris
                                                        nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                                                        in
                                                        reprehenderit in voluptate velit esse cillum dolore eu fugiat
                                                        nulla pariatur. Excepteur sint occaecat cupidatat non proident,
                                                        sunt in culpa qui officia deserunt mollit anim id est laborum.
                                                    </p>
                                                </div>
                                                <div class="link text-end">
                                                    <a href="" class="btn">Read More</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="beauty inner__padding bg-third">
            <div class="container">
                <div class="title">
                    <h2>beauty</h2>
                </div>
                <div class="sub__title">Hot Articles</div>
                <div class="beauty__items mt-3">
                    <div class="row">
                        <div class="col-lg-6">
                            <a href="" class="beauty__item bg__item d-block">
                                <div class="img">
                                    <img src="<?php echo $site_path; ?>/images/pic-1.jpg?w=675&h=464&mode=crop&scale=both"
                                        class="img-fluid" alt="" />
                                    <div class="data">
                                        <div class="heading">
                                            <h3>Nine Tips to Keep Your Skin Glowing</h3>
                                        </div>
                                        <div class="desc">
                                            <p>
                                                Winter doesn't have to mean dry skin! Indeed, these at-home
                                                remedies can help soothe even the most irritated, cracked
                                            </p>
                                        </div>
                                        <div class="date__author fw-lighter text-end">
                                            <span class="date">22 Dec 2021</span> -
                                            <span class="author">By Author Name</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-6 mt-lg-0 mt-md-3 mt-3">
                            <div class="row justify-content-center">
                                <div class="col-lg-12 col-md-6">
                                    <a href="" class="beauty__item side__item d-block">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="img">
                                                    <img src="<?php echo $site_path; ?>/images/pic-2.jpg?w=235&h=235&mode=crop&scale=both"
                                                        class="img-fluid" alt="" />
                                                </div>
                                            </div>
                                            <div class="col-lg-8">
                                                <div class="data">
                                                    <div class="heading">
                                                        <h3>10 Natural Dry-Skin Remedies You Can DIY at Home</h3>
                                                    </div>
                                                    <div class="desc">
                                                        <p>
                                                            Winter doesn't have to mean dry skin! Indeed, these at-home
                                                            remedies can help soothe even the most irritated, cracked
                                                        </p>
                                                    </div>
                                                    <div class="date__author fw-lighter text-primary">
                                                        <span class="date">22 Dec 2021</span> -
                                                        <span class="author">By Author Name</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-lg-12 col-md-6">
                                    <a href="" class="beauty__item side__item d-block">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="img">
                                                    <img src="<?php echo $site_path; ?>/images/pic-2.jpg?w=235&h=235&mode=crop&scale=both"
                                                        class="img-fluid" alt="" />
                                                </div>
                                            </div>
                                            <div class="col-lg-8">
                                                <div class="data">
                                                    <div class="heading">
                                                        <h3>10 Natural Dry-Skin Remedies You Can DIY at Home</h3>
                                                    </div>
                                                    <div class="desc">
                                                        <p>
                                                            Winter doesn't have to mean dry skin! Indeed, these at-home
                                                            remedies can help soothe even the most irritated, cracked
                                                        </p>
                                                    </div>
                                                    <div class="date__author fw-lighter text-primary">
                                                        <span class="date">22 Dec 2021</span> -
                                                        <span class="author">By Author Name</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 mt-lg-3 mt-0">
                            <div class="row justify-content-center">
                                <div class="col-lg-4 col-md-6">
                                    <a href="" class="beauty__item top__item d-block">
                                        <div class="img">
                                            <img src="<?php echo $site_path; ?>/images/pic-3.jpg?w=440&h=281&mode=crop&scale=both"
                                                class="img-fluid" alt="" />
                                        </div>
                                        <div class="data">
                                            <div class="heading">
                                                <h3>10 Natural Dry-Skin Remedies You Can DIY at Home</h3>
                                            </div>
                                            <div class="desc">
                                                <p>
                                                    Winter doesn't have to mean dry skin! Indeed, these at-home
                                                    remedies can help soothe even the most irritated, cracked
                                                </p>
                                            </div>
                                            <div class="date__author fw-lighter text-primary">
                                                <span class="date">22 Dec 2021</span> -
                                                <span class="author">By Author Name</span>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <a href="" class="beauty__item top__item d-block">
                                        <div class="img">
                                            <img src="<?php echo $site_path; ?>/images/pic-3.jpg?w=440&h=281&mode=crop&scale=both"
                                                class="img-fluid" alt="" />
                                        </div>
                                        <div class="data">
                                            <div class="heading">
                                                <h3>10 Natural Dry-Skin Remedies You Can DIY at Home</h3>
                                            </div>
                                            <div class="desc">
                                                <p>
                                                    Winter doesn't have to mean dry skin! Indeed, these at-home
                                                    remedies can help soothe even the most irritated, cracked
                                                </p>
                                            </div>
                                            <div class="date__author fw-lighter text-primary">
                                                <span class="date">22 Dec 2021</span> -
                                                <span class="author">By Author Name</span>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <a href="" class="beauty__item top__item d-block">
                                        <div class="img">
                                            <img src="<?php echo $site_path; ?>/images/pic-3.jpg?w=440&h=281&mode=crop&scale=both"
                                                class="img-fluid" alt="" />
                                        </div>
                                        <div class="data">
                                            <div class="heading">
                                                <h3>10 Natural Dry-Skin Remedies You Can DIY at Home</h3>
                                            </div>
                                            <div class="desc">
                                                <p>
                                                    Winter doesn't have to mean dry skin! Indeed, these at-home
                                                    remedies can help soothe even the most irritated, cracked
                                                </p>
                                            </div>
                                            <div class="date__author fw-lighter text-primary">
                                                <span class="date">22 Dec 2021</span> -
                                                <span class="author">By Author Name</span>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="wellness inner__padding">
            <div class="container">
                <div class="title">
                    <h2>wellness</h2>
                </div>
                <div class="sub__title">Top Secrets</div>
                <div class="wellness__items mt-3">
                    <div class="row">
                        <div class="col-12">
                            <a href="" class="wellness__item side__bg__item d-block">
                                <div class="row align-items-center">
                                    <div class="col-md-6">
                                        <div class="img">
                                            <img src="<?php echo $site_path; ?>/images/pic-2.jpg?w=675&h=377&mode=crop&scale=both"
                                                class="img-fluid" alt="" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="data">
                                            <div class="heading">
                                                <h3>10 Natural Dry-Skin Remedies You Can DIY at Home</h3>
                                            </div>
                                            <div class="desc">
                                                <p>
                                                    The recent U.S. Food and Drug Administration (FDA) approval of the
                                                    first-ever long-acting injectable HIV treatment may be a game
                                                    changer for patients who have had to depend on a daily pill to
                                                    manage their condition. The injectable formulation of cabotegravir
                                                    and rilpivirine — sold under the brand name Cabenuva — is meant
                                                    for patients with human immunodeficiency virus type 1 (HIV) who
                                                    are already virally suppressed
                                                </p>
                                            </div>
                                            <div class="date__author fw-lighter text-primary">
                                                <span class="date">22 Dec 2021</span> -
                                                <span class="author">By Author Name</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-12 mt-3">
                            <div class="row justify-content-center">
                                <div class="col-lg-4 col-md-6">
                                    <a href="" class="wellness__item top__item d-block">
                                        <div class="img">
                                            <img src="<?php echo $site_path; ?>/images/pic-3.jpg?w=440&h=281&mode=crop&scale=both"
                                                class="img-fluid" alt="" />
                                        </div>
                                        <div class="data">
                                            <div class="heading">
                                                <h3>10 Natural Dry-Skin Remedies You Can DIY at Home</h3>
                                            </div>
                                            <div class="desc">
                                                <p>
                                                    Winter doesn't have to mean dry skin! Indeed, these at-home
                                                    remedies can help soothe even the most irritated, cracked
                                                </p>
                                            </div>
                                            <div class="date__author fw-lighter text-primary">
                                                <span class="date">22 Dec 2021</span> -
                                                <span class="author">By Author Name</span>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <a href="" class="wellness__item top__item d-block">
                                        <div class="img">
                                            <img src="<?php echo $site_path; ?>/images/pic-3.jpg?w=440&h=281&mode=crop&scale=both"
                                                class="img-fluid" alt="" />
                                        </div>
                                        <div class="data">
                                            <div class="heading">
                                                <h3>10 Natural Dry-Skin Remedies You Can DIY at Home</h3>
                                            </div>
                                            <div class="desc">
                                                <p>
                                                    Winter doesn't have to mean dry skin! Indeed, these at-home
                                                    remedies can help soothe even the most irritated, cracked
                                                </p>
                                            </div>
                                            <div class="date__author fw-lighter text-primary">
                                                <span class="date">22 Dec 2021</span> -
                                                <span class="author">By Author Name</span>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <a href="" class="wellness__item top__item d-block">
                                        <div class="img">
                                            <img src="<?php echo $site_path; ?>/images/pic-3.jpg?w=440&h=281&mode=crop&scale=both"
                                                class="img-fluid" alt="" />
                                        </div>
                                        <div class="data">
                                            <div class="heading">
                                                <h3>10 Natural Dry-Skin Remedies You Can DIY at Home</h3>
                                            </div>
                                            <div class="desc">
                                                <p>
                                                    Winter doesn't have to mean dry skin! Indeed, these at-home
                                                    remedies can help soothe even the most irritated, cracked
                                                </p>
                                            </div>
                                            <div class="date__author fw-lighter text-primary">
                                                <span class="date">22 Dec 2021</span> -
                                                <span class="author">By Author Name</span>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="harmony inner__padding bg-third">
            <div class="container">
                <div class="title">
                    <h2>harmony</h2>
                </div>
                <div class="sub__title">Top Articles</div>
                <div class="harmony__items mt-3">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="row justify-content-center">
                                <div class="col-lg-12 col-md-6">
                                    <a href="" class="harmony__item side__item d-block">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="img">
                                                    <img src="<?php echo $site_path; ?>/images/pic-2.jpg?w=235&h=235&mode=crop&scale=both"
                                                        class="img-fluid" alt="" />
                                                </div>
                                            </div>
                                            <div class="col-lg-8">
                                                <div class="data">
                                                    <div class="heading">
                                                        <h3>10 Natural Dry-Skin Remedies You Can DIY at Home</h3>
                                                    </div>
                                                    <div class="desc">
                                                        <p>
                                                            Winter doesn't have to mean dry skin! Indeed, these at-home
                                                            remedies can help soothe even the most irritated, cracked
                                                        </p>
                                                    </div>
                                                    <div class="date__author fw-lighter text-primary">
                                                        <span class="date">22 Dec 2021</span> -
                                                        <span class="author">By Author Name</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-lg-12 col-md-6">
                                    <a href="" class="harmony__item side__item d-block">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="img">
                                                    <img src="<?php echo $site_path; ?>/images/pic-2.jpg?w=235&h=235&mode=crop&scale=both"
                                                        class="img-fluid" alt="" />
                                                </div>
                                            </div>
                                            <div class="col-lg-8">
                                                <div class="data">
                                                    <div class="heading">
                                                        <h3>10 Natural Dry-Skin Remedies You Can DIY at Home</h3>
                                                    </div>
                                                    <div class="desc">
                                                        <p>
                                                            Winter doesn't have to mean dry skin! Indeed, these at-home
                                                            remedies can help soothe even the most irritated, cracked
                                                        </p>
                                                    </div>
                                                    <div class="date__author fw-lighter text-primary">
                                                        <span class="date">22 Dec 2021</span> -
                                                        <span class="author">By Author Name</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 mt-lg-0 mt-md-3 mt-0">
                            <a href="" class="harmony__item bg__item d-block">
                                <div class="img">
                                    <img src="<?php echo $site_path; ?>/images/pic-1.jpg?w=675&h=464&mode=crop&scale=both"
                                        class="img-fluid" alt="" />
                                    <div class="data">
                                        <div class="heading">
                                            <h3>Nine Tips to Keep Your Skin Glowing</h3>
                                        </div>
                                        <div class="desc">
                                            <p>
                                                Winter doesn't have to mean dry skin! Indeed, these at-home
                                                remedies can help soothe even the most irritated, cracked
                                            </p>
                                        </div>
                                        <div class="date__author fw-lighter text-end">
                                            <span class="date">22 Dec 2021</span> -
                                            <span class="author">By Author Name</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-12 mt-3">
                            <div class="row justify-content-center">
                                <div class="col-lg-4 col-md-6">
                                    <a href="" class="harmony__item top__item d-block">
                                        <div class="img">
                                            <img src="<?php echo $site_path; ?>/images/pic-3.jpg?w=440&h=281&mode=crop&scale=both"
                                                class="img-fluid" alt="" />
                                        </div>
                                        <div class="data">
                                            <div class="heading">
                                                <h3>10 Natural Dry-Skin Remedies You Can DIY at Home</h3>
                                            </div>
                                            <div class="desc">
                                                <p>
                                                    Winter doesn't have to mean dry skin! Indeed, these at-home
                                                    remedies can help soothe even the most irritated, cracked
                                                </p>
                                            </div>
                                            <div class="date__author fw-lighter text-primary">
                                                <span class="date">22 Dec 2021</span> -
                                                <span class="author">By Author Name</span>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <a href="" class="harmony__item top__item d-block">
                                        <div class="img">
                                            <img src="<?php echo $site_path; ?>/images/pic-3.jpg?w=440&h=281&mode=crop&scale=both"
                                                class="img-fluid" alt="" />
                                        </div>
                                        <div class="data">
                                            <div class="heading">
                                                <h3>10 Natural Dry-Skin Remedies You Can DIY at Home</h3>
                                            </div>
                                            <div class="desc">
                                                <p>
                                                    Winter doesn't have to mean dry skin! Indeed, these at-home
                                                    remedies can help soothe even the most irritated, cracked
                                                </p>
                                            </div>
                                            <div class="date__author fw-lighter text-primary">
                                                <span class="date">22 Dec 2021</span> -
                                                <span class="author">By Author Name</span>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <a href="" class="harmony__item top__item d-block">
                                        <div class="img">
                                            <img src="<?php echo $site_path; ?>/images/pic-3.jpg?w=440&h=281&mode=crop&scale=both"
                                                class="img-fluid" alt="" />
                                        </div>
                                        <div class="data">
                                            <div class="heading">
                                                <h3>10 Natural Dry-Skin Remedies You Can DIY at Home</h3>
                                            </div>
                                            <div class="desc">
                                                <p>
                                                    Winter doesn't have to mean dry skin! Indeed, these at-home
                                                    remedies can help soothe even the most irritated, cracked
                                                </p>
                                            </div>
                                            <div class="date__author fw-lighter text-primary">
                                                <span class="date">22 Dec 2021</span> -
                                                <span class="author">By Author Name</span>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="shop inner__padding">
            <div class="container">
                <div class="title">
                    <h2>Shop</h2>
                </div>
                <div class="sub__title">Best Seller Products</div>
                <div class="product__items mt-3">
                    <div class="product__slider">
                        <div>
                            <div class="product__item">
                                <div class="img">
                                    <img src="<?php echo $site_path; ?>/images/pro-1.jpg?w=322&h=322&mode=crop&scale=both"
                                        class="img-fluid" alt="" /><button class="add__cart border-0 p-0">
                                        <img src="<?php echo $site_path; ?>/images/cart.svg" class="svg" alt="" />
                                    </button>
                                </div>
                                <a href="" class="d-block">
                                    <div class="heading__price">
                                        <div class="heading">
                                            <h3>Product title goes here</h3>
                                        </div>
                                        <div class="price">
                                            <div class="original text-primary">$100</div>
                                            <div class="original text-light-grey text-decoration-line-through">
                                                $100
                                            </div>
                                        </div>
                                    </div>
                                    <div class="category">Beauty</div>
                                </a>
                            </div>
                        </div>
                        <div>
                            <div class="product__item">
                                <div class="img">
                                    <img src="<?php echo $site_path; ?>/images/pro-1.jpg?w=322&h=322&mode=crop&scale=both"
                                        class="img-fluid" alt="" /><button class="add__cart border-0 p-0">
                                        <img src="<?php echo $site_path; ?>/images/cart.svg" class="svg" alt="" />
                                    </button>
                                </div>
                                <a href="" class="d-block">
                                    <div class="heading__price">
                                        <div class="heading">
                                            <h3>Product title goes here</h3>
                                        </div>
                                        <div class="price">
                                            <div class="original text-primary">$100</div>
                                            <div class="original text-light-grey text-decoration-line-through">
                                                $100
                                            </div>
                                        </div>
                                    </div>
                                    <div class="category">Beauty</div>
                                </a>
                            </div>
                        </div>
                        <div>
                            <div class="product__item">
                                <div class="img">
                                    <img src="<?php echo $site_path; ?>/images/pro-1.jpg?w=322&h=322&mode=crop&scale=both"
                                        class="img-fluid" alt="" /><button class="add__cart border-0 p-0">
                                        <img src="<?php echo $site_path; ?>/images/cart.svg" class="svg" alt="" />
                                    </button>
                                </div>
                                <a href="" class="d-block">
                                    <div class="heading__price">
                                        <div class="heading">
                                            <h3>Product title goes here</h3>
                                        </div>
                                        <div class="price">
                                            <div class="original text-primary">$100</div>
                                            <div class="original text-light-grey text-decoration-line-through">
                                                $100
                                            </div>
                                        </div>
                                    </div>
                                    <div class="category">Beauty</div>
                                </a>
                            </div>
                        </div>
                        <div>
                            <div class="product__item">
                                <div class="img">
                                    <img src="<?php echo $site_path; ?>/images/pro-1.jpg?w=322&h=322&mode=crop&scale=both"
                                        class="img-fluid" alt="" /><button class="add__cart border-0 p-0">
                                        <img src="<?php echo $site_path; ?>/images/cart.svg" class="svg" alt="" />
                                    </button>
                                </div>
                                <a href="" class="d-block">
                                    <div class="heading__price">
                                        <div class="heading">
                                            <h3>Product title goes here</h3>
                                        </div>
                                        <div class="price">
                                            <div class="original text-primary">$100</div>
                                            <div class="original text-light-grey text-decoration-line-through">
                                                $100
                                            </div>
                                        </div>
                                    </div>
                                    <div class="category">Beauty</div>
                                </a>
                            </div>
                        </div>
                        <div>
                            <div class="product__item">
                                <div class="img">
                                    <img src="<?php echo $site_path; ?>/images/pro-1.jpg?w=322&h=322&mode=crop&scale=both"
                                        class="img-fluid" alt="" /><button class="add__cart border-0 p-0">
                                        <img src="<?php echo $site_path; ?>/images/cart.svg" class="svg" alt="" />
                                    </button>
                                </div>
                                <a href="" class="d-block">
                                    <div class="heading__price">
                                        <div class="heading">
                                            <h3>Product title goes here</h3>
                                        </div>
                                        <div class="price">
                                            <div class="original text-primary">$100</div>
                                            <div class="original text-light-grey text-decoration-line-through">
                                                $100
                                            </div>
                                        </div>
                                    </div>
                                    <div class="category">Beauty</div>
                                </a>
                            </div>
                        </div>
                        <div>
                            <div class="product__item">
                                <div class="img">
                                    <img src="<?php echo $site_path; ?>/images/pro-1.jpg?w=322&h=322&mode=crop&scale=both"
                                        class="img-fluid" alt="" /><button class="add__cart border-0 p-0">
                                        <img src="<?php echo $site_path; ?>/images/cart.svg" class="svg" alt="" />
                                    </button>
                                </div>
                                <a href="" class="d-block">
                                    <div class="heading__price">
                                        <div class="heading">
                                            <h3>Product title goes here</h3>
                                        </div>
                                        <div class="price">
                                            <div class="original text-primary">$100</div>
                                            <div class="original text-light-grey text-decoration-line-through">
                                                $100
                                            </div>
                                        </div>
                                    </div>
                                    <div class="category">Beauty</div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="food inner__padding bg-third">
            <div class="container">
                <div class="title">
                    <h2>Food & Sprits</h2>
                </div>
                <div class="sub__title">Life Balance & Diet</div>
                <div class="food__items mt-3">
                    <div class="row">
                        <div class="col-12">
                            <div class="row justify-content-center">
                                <div class="col-lg-4 col-md-6">
                                    <a href="" class="food__item top__item d-block">
                                        <div class="img">
                                            <img src="<?php echo $site_path; ?>/images/pic-4.jpg?w=440&h=539&mode=crop&scale=both"
                                                class="img-fluid" alt="" />
                                        </div>
                                        <div class="data">
                                            <div class="heading">
                                                <h3>10 Natural Dry-Skin Remedies You Can DIY at Home</h3>
                                            </div>
                                            <div class="desc">
                                                <p>
                                                    Winter doesn't have to mean dry skin! Indeed, these at-home
                                                    remedies can help soothe even the most irritated, cracked
                                                </p>
                                            </div>
                                            <div class="date__author fw-lighter text-primary">
                                                <span class="date">22 Dec 2021</span> -
                                                <span class="author">By Author Name</span>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <a href="" class="food__item top__item d-block">
                                        <div class="img">
                                            <img src="<?php echo $site_path; ?>/images/pic-4.jpg?w=440&h=539&mode=crop&scale=both"
                                                class="img-fluid" alt="" />
                                        </div>
                                        <div class="data">
                                            <div class="heading">
                                                <h3>10 Natural Dry-Skin Remedies You Can DIY at Home</h3>
                                            </div>
                                            <div class="desc">
                                                <p>
                                                    Winter doesn't have to mean dry skin! Indeed, these at-home
                                                    remedies can help soothe even the most irritated, cracked
                                                </p>
                                            </div>
                                            <div class="date__author fw-lighter text-primary">
                                                <span class="date">22 Dec 2021</span> -
                                                <span class="author">By Author Name</span>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <a href="" class="food__item top__item d-block">
                                        <div class="img">
                                            <img src="<?php echo $site_path; ?>/images/pic-4.jpg?w=440&h=539&mode=crop&scale=both"
                                                class="img-fluid" alt="" />
                                        </div>
                                        <div class="data">
                                            <div class="heading">
                                                <h3>10 Natural Dry-Skin Remedies You Can DIY at Home</h3>
                                            </div>
                                            <div class="desc">
                                                <p>
                                                    Winter doesn't have to mean dry skin! Indeed, these at-home
                                                    remedies can help soothe even the most irritated, cracked
                                                </p>
                                            </div>
                                            <div class="date__author fw-lighter text-primary">
                                                <span class="date">22 Dec 2021</span> -
                                                <span class="author">By Author Name</span>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 mt-lg-3 mt-md-0">
                            <div class="row justify-content-center">
                                <div class="col-lg-4 col-md-6">
                                    <a href="" class="food__item top__item d-block">
                                        <div class="img">
                                            <img src="<?php echo $site_path; ?>/images/pic-3.jpg?w=440&h=170&mode=crop&scale=both"
                                                class="img-fluid" alt="" />
                                        </div>
                                        <div class="data">
                                            <div class="heading">
                                                <h3>10 Natural Dry-Skin Remedies You Can DIY at Home</h3>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <a href="" class="food__item top__item d-block">
                                        <div class="img">
                                            <img src="<?php echo $site_path; ?>/images/pic-3.jpg?w=440&h=170&mode=crop&scale=both"
                                                class="img-fluid" alt="" />
                                        </div>
                                        <div class="data">
                                            <div class="heading">
                                                <h3>10 Natural Dry-Skin Remedies You Can DIY at Home</h3>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <a href="" class="food__item top__item d-block">
                                        <div class="img">
                                            <img src="<?php echo $site_path; ?>/images/pic-3.jpg?w=440&h=170&mode=crop&scale=both"
                                                class="img-fluid" alt="" />
                                        </div>
                                        <div class="data">
                                            <div class="heading">
                                                <h3>10 Natural Dry-Skin Remedies You Can DIY at Home</h3>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="travel inner__padding pb-3">
            <div class="container">
                <div class="title">
                    <h2>Travel</h2>
                </div>
                <div class="sub__title">Trending</div>
                <div class="travel__items mt-3">
                    <div class="row justify-content-center" data-masonry='{"percentPosition": true }'>
                        <div class="col-lg-4 col-md-6">
                            <a href="" class="travel__team top__item d-block">
                                <div class="img">
                                    <img src="<?php echo $site_path; ?>/images/pic-5.jpg?w=440&h=281&mode=crop&scale=both"
                                        class="img-fluid" alt="" />
                                </div>
                                <div class="data">
                                    <div class="heading">
                                        <h3>10 Natural Dry-Skin Remedies You Can DIY at Home</h3>
                                    </div>
                                    <div class="desc">
                                        <p>
                                            Winter doesn't have to mean dry skin! Indeed, these at-home remedies
                                            can help soothe even the most irritated, cracked
                                        </p>
                                    </div>
                                    <div class="date__author fw-lighter text-primary">
                                        <span class="date">22 Dec 2021</span> -
                                        <span class="author">By Author Name</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <a href="" class="beauty__item bg__item d-block">
                                <div class="img">
                                    <img src="<?php echo $site_path; ?>/images/pic-4.jpg?w=675&h=429&mode=crop&scale=both"
                                        class="img-fluid" alt="" />
                                    <div class="data">
                                        <div class="heading">
                                            <h3>Nine Tips to Keep Your Skin Glowing</h3>
                                        </div>
                                        <div class="desc">
                                            <p>
                                                Winter doesn't have to mean dry skin! Indeed, these at-home
                                                remedies can help soothe even the most irritated, cracked
                                            </p>
                                        </div>
                                        <div class="date__author fw-lighter text-end">
                                            <span class="date">22 Dec 2021</span> -
                                            <span class="author">By Author Name</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <a href="" class="travel__team top__item d-block">
                                <div class="img">
                                    <img src="<?php echo $site_path; ?>/images/pic-5.jpg?w=440&h=281&mode=crop&scale=both"
                                        class="img-fluid" alt="" />
                                </div>
                                <div class="data">
                                    <div class="heading">
                                        <h3>10 Natural Dry-Skin Remedies You Can DIY at Home</h3>
                                    </div>
                                    <div class="desc">
                                        <p>
                                            Winter doesn't have to mean dry skin! Indeed, these at-home remedies
                                            can help soothe even the most irritated, cracked
                                        </p>
                                    </div>
                                    <div class="date__author fw-lighter text-primary">
                                        <span class="date">22 Dec 2021</span> -
                                        <span class="author">By Author Name</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <a href="" class="travel__team top__item d-block">
                                <div class="img">
                                    <img src="<?php echo $site_path; ?>/images/pic-5.jpg?w=440&h=281&mode=crop&scale=both"
                                        class="img-fluid" alt="" />
                                </div>
                                <div class="data">
                                    <div class="heading">
                                        <h3>10 Natural Dry-Skin Remedies You Can DIY at Home</h3>
                                    </div>
                                    <div class="desc">
                                        <p>
                                            Winter doesn't have to mean dry skin! Indeed, these at-home remedies
                                            can help soothe even the most irritated, cracked
                                        </p>
                                    </div>
                                    <div class="date__author fw-lighter text-primary">
                                        <span class="date">22 Dec 2021</span> -
                                        <span class="author">By Author Name</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <a href="" class="travel__team top__item d-block">
                                <div class="img">
                                    <img src="<?php echo $site_path; ?>/images/pic-5.jpg?w=440&h=281&mode=crop&scale=both"
                                        class="img-fluid" alt="" />
                                </div>
                                <div class="data">
                                    <div class="heading">
                                        <h3>10 Natural Dry-Skin Remedies You Can DIY at Home</h3>
                                    </div>
                                    <div class="desc">
                                        <p>
                                            Winter doesn't have to mean dry skin! Indeed, these at-home remedies
                                            can help soothe even the most irritated, cracked
                                        </p>
                                    </div>
                                    <div class="date__author fw-lighter text-primary">
                                        <span class="date">22 Dec 2021</span> -
                                        <span class="author">By Author Name</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <a href="" class="beauty__item bg__item d-block">
                                <div class="img">
                                    <img src="<?php echo $site_path; ?>/images/pic-4.jpg?w=675&h=429&mode=crop&scale=both"
                                        class="img-fluid" alt="" />
                                    <div class="data">
                                        <div class="heading">
                                            <h3>Nine Tips to Keep Your Skin Glowing</h3>
                                        </div>
                                        <div class="desc">
                                            <p>
                                                Winter doesn't have to mean dry skin! Indeed, these at-home
                                                remedies can help soothe even the most irritated, cracked
                                            </p>
                                        </div>
                                        <div class="date__author fw-lighter text-end">
                                            <span class="date">22 Dec 2021</span> -
                                            <span class="author">By Author Name</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="book inner__padding bg-third pb-3">
            <div class="container">
                <div class="title">
                    <h2>Book Club</h2>
                </div>
                <div class="sub__title">Top Read Book</div>
                <div class="book__items mt-3">
                    <div class="row justify-content-center">
                        <div class="col-lg-4 col-md-6">
                            <a href="" class="book__item side__item d-block">
                                <div class="row">
                                    <div class="col-4">
                                        <div class="img">
                                            <img src="<?php echo $site_path; ?>/images/pic-6.jpg?w=139&h=206&mode=crop&scale=both"
                                                class="img-fluid" alt="" />
                                        </div>
                                    </div>
                                    <div class="col-8">
                                        <div class="data">
                                            <div class="heading">
                                                <h3>The Most Difficult Road</h3>
                                            </div>
                                            <div class="desc">
                                                <p>
                                                    MIA ΔIAΣHMH ΓYNAIKA ΣTO ΔPOMO THΣ EΠIΣTPOΦHΣ. O MYΘOΣ THΣ. H
                                                    ANOΔOΣ KAI H ΠTΩΣH MIAΣ AΠO TIΣ
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <a href="" class="book__item side__item d-block">
                                <div class="row">
                                    <div class="col-4">
                                        <div class="img">
                                            <img src="<?php echo $site_path; ?>/images/pic-6.jpg?w=139&h=206&mode=crop&scale=both"
                                                class="img-fluid" alt="" />
                                        </div>
                                    </div>
                                    <div class="col-8">
                                        <div class="data">
                                            <div class="heading">
                                                <h3>The Most Difficult Road</h3>
                                            </div>
                                            <div class="desc">
                                                <p>
                                                    MIA ΔIAΣHMH ΓYNAIKA ΣTO ΔPOMO THΣ EΠIΣTPOΦHΣ. O MYΘOΣ THΣ. H
                                                    ANOΔOΣ KAI H ΠTΩΣH MIAΣ AΠO TIΣ
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <a href="" class="book__item side__item d-block">
                                <div class="row">
                                    <div class="col-4">
                                        <div class="img">
                                            <img src="<?php echo $site_path; ?>/images/pic-6.jpg?w=139&h=206&mode=crop&scale=both"
                                                class="img-fluid" alt="" />
                                        </div>
                                    </div>
                                    <div class="col-8">
                                        <div class="data">
                                            <div class="heading">
                                                <h3>The Most Difficult Road</h3>
                                            </div>
                                            <div class="desc">
                                                <p>
                                                    MIA ΔIAΣHMH ΓYNAIKA ΣTO ΔPOMO THΣ EΠIΣTPOΦHΣ. O MYΘOΣ THΣ. H
                                                    ANOΔOΣ KAI H ΠTΩΣH MIAΣ AΠO TIΣ
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <a href="" class="book__item side__item d-block">
                                <div class="row">
                                    <div class="col-4">
                                        <div class="img">
                                            <img src="<?php echo $site_path; ?>/images/pic-6.jpg?w=139&h=206&mode=crop&scale=both"
                                                class="img-fluid" alt="" />
                                        </div>
                                    </div>
                                    <div class="col-8">
                                        <div class="data">
                                            <div class="heading">
                                                <h3>The Most Difficult Road</h3>
                                            </div>
                                            <div class="desc">
                                                <p>
                                                    MIA ΔIAΣHMH ΓYNAIKA ΣTO ΔPOMO THΣ EΠIΣTPOΦHΣ. O MYΘOΣ THΣ. H
                                                    ANOΔOΣ KAI H ΠTΩΣH MIAΣ AΠO TIΣ
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <a href="" class="book__item side__item d-block">
                                <div class="row">
                                    <div class="col-4">
                                        <div class="img">
                                            <img src="<?php echo $site_path; ?>/images/pic-6.jpg?w=139&h=206&mode=crop&scale=both"
                                                class="img-fluid" alt="" />
                                        </div>
                                    </div>
                                    <div class="col-8">
                                        <div class="data">
                                            <div class="heading">
                                                <h3>The Most Difficult Road</h3>
                                            </div>
                                            <div class="desc">
                                                <p>
                                                    MIA ΔIAΣHMH ΓYNAIKA ΣTO ΔPOMO THΣ EΠIΣTPOΦHΣ. O MYΘOΣ THΣ. H
                                                    ANOΔOΣ KAI H ΠTΩΣH MIAΣ AΠO TIΣ
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <a href="" class="book__item side__item d-block">
                                <div class="row">
                                    <div class="col-4">
                                        <div class="img">
                                            <img src="<?php echo $site_path; ?>/images/pic-6.jpg?w=139&h=206&mode=crop&scale=both"
                                                class="img-fluid" alt="" />
                                        </div>
                                    </div>
                                    <div class="col-8">
                                        <div class="data">
                                            <div class="heading">
                                                <h3>The Most Difficult Road</h3>
                                            </div>
                                            <div class="desc">
                                                <p>
                                                    MIA ΔIAΣHMH ΓYNAIKA ΣTO ΔPOMO THΣ EΠIΣTPOΦHΣ. O MYΘOΣ THΣ. H
                                                    ANOΔOΣ KAI H ΠTΩΣH MIAΣ AΠO TIΣ
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include '../../component/footer.php';?>
    <?php include '../../component/scripts.php';?>
</body>

</html>