<!DOCTYPE html>
<?php
$current_page ='About';
$timestamp = date("YmdHis");
// $site_path = 'https://www.creamyw.com/dev/zinaclub/assets';
$site_path = 'http://localhost/zinaclub/assets';
// $site_url = 'https://www.creamyw.com/dev/zinaclub/views';
$site_url = 'http://localhost/zinaclub/views';
?>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>ZinaClub | <?php echo $current_page ?></title>
    <?php include '../../component/head.php';?>
</head>

<body>
    <?php include '../../component/navbar.php';?>
    <div class="body__wraper">
        <div class="internal__header">
            <div class="container text-center">
                <div class="heading">
                    <h1>
                        <?php echo $current_page ?>
                    </h1>
                </div>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb justify-content-center">
                        <li class="breadcrumb-item"><a href="<?php echo $site_url ?>/home/">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page"> <?php echo $current_page ?></li>
                    </ol>
                </nav>
            </div>
        </div>

        <div class="inner__padding about__page">
            <div class="container">
                <div class="row mt-3">
                    <div class="col-lg-8 col-md-10 mx-auto">
                        <div class="title text-center">
                            <h2>What’s zina club?</h2>
                        </div>
                        <div class="desc mt-3 text-center">
                            <p>Quisque sollicitudin lacinia sapien, eu tincidunt nunc accumsan laoreet. Curabitur
                                feugiat
                                posuere odio nec tincidunt. Pellentesque sagittis nibh venenatis sapieni congue
                                consectetur.
                                Integer nulla nunc, efficitur sit amet sagittis sed, suscipit et magna. Nulla porttitor
                                neque
                                non dapibus nec elit tristique sagittis.</p>
                            <div class="sub__title text-primary mb-2">
                                <h5>“Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos
                                    himenaeos.
                                    Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit.”
                                </h5>
                            </div>
                            <p>
                                Pellentesque sagittis nibh venenatis sapieni congue consectetur. Integer nulla nunc,
                                efficitur sit amet sagittis sed, suscipit et magna. Nulla porttitor neque non dapibus
                                nec elit tristique sagittis.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="inner__padding bg-third about__page">
            <div class="container">
                <div class="title text-center">
                    <h2>What zina club provides?</h2>
                </div>
                <div class="about__data mt-lg-5 mt-md-4 mt-3">
                    <div class="row">
                        <div class="col-lg-10 mx-auto">
                            <div class="about__item">
                                <div class="row">
                                    <div class="col-lg-5 col-md-6">
                                        <div class="img">
                                            <img src="<?php echo $site_path ?>/images/pic-8.jpg?w=440&h=322&mode=crop&scale=both"
                                                class="img-fluid w-100" alt="">
                                            <div class="num">01</div>
                                        </div>
                                    </div>
                                    <div class="col-lg-7 col-md-6">
                                        <div class="sub__title  mt-md-0 mt-3">
                                            <h3>ARTICLES</h3>
                                        </div>
                                        <div class="desc mt-3">
                                            <p>We take a curious, open-minded, and service-centric approach to the work
                                                we do. We ask questions about all of it. We believe that people can take
                                                what serves them and leave what doesn’t. We recommend what we love and
                                                what we think is worthy of your time and wallet. We value your trust
                                                above all things.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="about__item">
                                <div class="row">
                                    <div class="col-lg-5 col-md-6">
                                        <div class="img">
                                            <img src="<?php echo $site_path ?>/images/pic-8.jpg?w=440&h=322&mode=crop&scale=both"
                                                class="img-fluid w-100" alt="">
                                            <div class="num">02</div>
                                        </div>
                                    </div>
                                    <div class="col-lg-7 col-md-6">
                                        <div class="sub__title mt-md-0 mt-3">
                                            <h3>ARTICLES</h3>
                                        </div>
                                        <div class="desc mt-3">
                                            <p>We take a curious, open-minded, and service-centric approach to the work
                                                we do. We ask questions about all of it. We believe that people can take
                                                what serves them and leave what doesn’t. We recommend what we love and
                                                what we think is worthy of your time and wallet. We value your trust
                                                above all things.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include '../../component/footer.php';?>
    <?php include '../../component/scripts.php';?>
</body>

</html>