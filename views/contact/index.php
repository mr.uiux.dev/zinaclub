<!DOCTYPE html>
<?php
$current_page ='Contact';
$timestamp = date("YmdHis");
// $site_path = 'https://www.creamyw.com/dev/zinaclub/assets';
$site_path = 'http://localhost/zinaclub/assets';
// $site_url = 'https://www.creamyw.com/dev/zinaclub/views';
$site_url = 'http://localhost/zinaclub/views';
?>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>ZinaClub | <?php echo $current_page ?></title>
    <?php include '../../component/head.php';?>
</head>

<body>
    <?php include '../../component/navbar.php';?>
    <div class="body__wraper">
        <div class="internal__header">
            <div class="container text-center">
                <div class="heading">
                    <h1>
                        <?php echo $current_page ?>
                    </h1>
                </div>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb justify-content-center">
                        <li class="breadcrumb-item"><a href="<?php echo $site_url ?>/home/">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page"> <?php echo $current_page ?></li>
                    </ol>
                </nav>
            </div>
        </div>

        <div class="contact__page">
            <div class="container p-0">
                <div class="row g-0">
                    <div class="col-xl-5 col-lg-6 left__side">
                        <div class="title">
                            <h2>contact information</h2>
                        </div>
                        <div class="info">
                            <div class="address">
                                <div class="sub__title">
                                    <h3>Visit our Company at</h3>
                                </div>
                                <a href="" class="d-block mt-2">2005 Stokes Isle Apt. 896,
                                    Vacaville 10010, USA</a>
                            </div>
                            <div class="message">
                                <div class="sub__title">
                                    <h3>Message us</h3>
                                </div>
                                <a href="mailto:info@zinaclub.com" class="d-block mt-2">info@zinaclub.com</a>
                                <a href="tel:0068120034509" class="d-block mt-2">(+68) 120034509</a>
                            </div>
                            <div class="social__media">
                                <div class="sub__title">
                                    <h3>Follow Us</h3>
                                </div>
                                <div class="social">
                                    <ul class="list-inline mb-0">
                                        <li>
                                            <a href="" target="_blank">
                                                <i class="fab fa-facebook-f"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="" target="_blank">
                                                <i class="fab fa-twitter"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="" target="_blank">
                                                <i class="fab fa-youtube"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="" target="_blank">
                                                <i class="fab fa-instagram"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="" target="_blank">
                                                <i class="fab fa-pinterest-p"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-7 col-lg-6 ps-lg-3 px-md-2 px-2 inner__padding">
                        <div class="title">
                            <h2>get in touch</h2>
                        </div>
                        <div class="form">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Full Name</label>
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Email Address</label>
                                        <input type="email" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Phone Number</label>
                                        <input type="tel" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Subject</label>
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="">Message</label>
                                        <textarea name="" id="" rows="10" class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="col-12 text-end">
                                    <button type="submit" class="btn">Send Message</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include '../../component/footer.php';?>
    <?php include '../../component/scripts.php';?>
</body>

</html>