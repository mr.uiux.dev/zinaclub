<!DOCTYPE html>
<?php
$current_page ='Checkout Details';
$timestamp = date("YmdHis");
// $site_path = 'https://www.creamyw.com/dev/zinaclub/assets';
$site_path = 'http://localhost/zinaclub/assets';
// $site_url = 'https://www.creamyw.com/dev/zinaclub/views';
$site_url = 'http://localhost/zinaclub/views';
?>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>ZinaClub | <?php echo $current_page ?></title>
    <?php include '../../component/head.php';?>
</head>

<body>
    <?php include '../../component/navbar.php';?>
    <div class="body__wraper">
        <div class="internal__header">
            <div class="container text-center">
                <div class="heading">
                    <h1>
                        <?php echo $current_page ?>
                    </h1>
                </div>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb justify-content-center">
                        <li class="breadcrumb-item"><a href="<?php echo $site_url ?>/home/">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page"> <?php echo $current_page ?></li>
                    </ol>
                </nav>
            </div>
        </div>

        <div class="cart__page inner__padding">
            <div class="container">
                <div class="row">
                    <div class="col-xl-8 col-lg-7 col-md-6 theia">
                        <div class="contact__page">
                            <div class="title">
                                <h2>Billing informations</h2>
                            </div>
                            <div class="form">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="">Full Name</label>
                                            <input type="text" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="">Email Address</label>
                                            <input type="email" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="">Adjective</label>
                                            <input type="text" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="">Country</label>
                                            <select name="" id="" class="form-control">
                                                <option value="Greece">Greece</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="">Address</label>
                                            <input type="text" class="form-control" placeholder="Address and number">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for=""></label>
                                            <input type="text" class="form-control"
                                                placeholder="Apartment, building, etc. (optional)">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="">Town/City</label>
                                            <input type="text" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="">Regoin(Optional)</label>
                                            <select name="" id="" class="form-control">
                                                <option value="Greece">Make a choice</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="">Zip Code/ZIP</label>
                                            <input type="text" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="">Phone Number</label>
                                            <input type="tel" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="">Type</label>
                                            <select name="" id="" class="form-control">
                                                <option value="Greece">Invoice</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-check form-group">
                                            <input class="form-check-input" type="checkbox" value=""
                                                id="flexCheckDefault">
                                            <label class="form-check-label" for="flexCheckDefault">
                                                Send to a different address</a>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="">Full Name</label>
                                            <input type="text" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="">Email Address</label>
                                            <input type="email" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="">Adjective</label>
                                            <input type="text" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="">Country</label>
                                            <select name="" id="" class="form-control">
                                                <option value="Greece">Greece</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="">Address</label>
                                            <input type="text" class="form-control" placeholder="Address and number">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for=""></label>
                                            <input type="text" class="form-control"
                                                placeholder="Apartment, building, etc. (optional)">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="">Town/City</label>
                                            <input type="text" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="">Regoin(Optional)</label>
                                            <select name="" id="" class="form-control">
                                                <option value="Greece">Make a choice</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="">Zip Code/ZIP</label>
                                            <input type="text" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label for="">Phone Number</label>
                                            <input type="tel" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="">Type</label>
                                            <select name="" id="" class="form-control">
                                                <option value="Greece">Invoice</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-5 col-md-6 theia">
                        <div class=" cart__total mt-0">
                            <div class="sub__title mb-1">
                                <h3>Order Summary</h3>
                            </div>
                            <div class="products__list mt-2">
                                <a href="" class="product__cart d-block">
                                    <div class="row">
                                        <div class="col-3 pe-0">
                                            <div class="img">
                                                <img src="<?php echo $site_path; ?>/images/pro-1.jpg?w=137&h=137&scale=both&mode=crop"
                                                    class="img-fluid" alt="" />
                                            </div>
                                        </div>
                                        <div class="col-9">
                                            <div class="d-flex">
                                                <div class="title">Assertively fabricate top-line products</div>
                                                <div class="price">$240</div>
                                            </div>
                                            <div class="qty mt-1">
                                                QTY: 8
                                            </div>
                                        </div>
                                    </div>
                                </a>
                                <a href="" class="product__cart d-block">
                                    <div class="row">
                                        <div class="col-3 pe-0">
                                            <div class="img">
                                                <img src="<?php echo $site_path; ?>/images/pro-1.jpg?w=137&h=137&scale=both&mode=crop"
                                                    class="img-fluid" alt="" />
                                            </div>
                                        </div>
                                        <div class="col-9">
                                            <div class="d-flex">
                                                <div class="title">Assertively fabricate top-line products</div>
                                                <div class="price">$240</div>
                                            </div>
                                            <div class="qty mt-1">
                                                QTY: 8
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="info__price">
                                <div class="subtotal d-flex justify-content-between">
                                    <span>Subtotal</span>
                                    <span>$280</span>
                                </div>
                                <div class="total mt-2 d-flex justify-content-between">
                                    <span>Total</span>
                                    <span class="text-primary">$280</span>
                                </div>
                                <div class="tax mt-2 text-end">
                                    (includes €2.71 VAT calculated for Greece)
                                </div>
                            </div>
                            <div class="payment">
                                <div class="paypal">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="visa" id="paypal" checked>
                                        <label
                                            class="form-check-label text-secondary d-flex align-items-center justify-content-between"
                                            for="paypal">
                                            <div>
                                                PayPal <small>What’s PayPal?</small>
                                            </div>
                                            <img src="<?php echo $site_path ?>/images/paypal.png" alt="">
                                        </label>
                                        <div class="desc mt-1">
                                            <small>Payment by PayPal. You can pay with your credit card if you don't
                                                have an
                                                account on PayPal.</small>
                                        </div>
                                    </div>
                                </div>
                                <div class="visas">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="visa" id="visas">
                                        <label
                                            class="form-check-label text-secondary d-flex align-items-center justify-content-between"
                                            for="visas">
                                            <div>
                                                Eurobank
                                            </div>
                                            <img src="<?php echo $site_path ?>/images/visas.png" alt="">
                                        </label>
                                        <div class="desc mt-1">
                                            <small>Pay securely by credit or debit card through Eurobank..</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="agreement">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault">
                                    <label class="form-check-label" for="flexCheckDefault">
                                        I have read and agree with the <a href="" class="text-primary">terms and
                                            conditions of the
                                            website *</a>
                                    </label>
                                </div>
                            </div>
                            <div class="checkout mt-2">
                                <a href="" class="btn btn-primary d-block">Send Order</a>
                            </div>
                            <div class="desc mt-1">
                                <small>Your personal data will be used to process your order, support your experience
                                    throughout this website and for other purposes described in our privacy
                                    policy*.</small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include '../../component/footer.php';?>
        <?php include '../../component/scripts.php';?>
</body>

</html>