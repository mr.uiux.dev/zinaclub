<!DOCTYPE html>
<?php
$current_page ='Shop';
$timestamp = date("YmdHis");
// $site_path = 'https://www.creamyw.com/dev/zinaclub/assets';
$site_path = 'http://localhost/zinaclub/assets';
// $site_url = 'https://www.creamyw.com/dev/zinaclub/views';
$site_url = 'http://localhost/zinaclub/views';
?>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>ZinaClub | <?php echo $current_page ?></title>
    <?php include '../../component/head.php';?>
</head>

<body>
    <?php include '../../component/navbar.php';?>
    <div class="body__wraper">
        <div class="details__shop__page inner__padding">
            <div class="container">
                <div class="row">
                    <div class="col-xl-4 col-lg-5 col-md-6">
                        <div class="product__slider__for">
                            <div>
                                <img src="<?php echo $site_path ?>/images/pro-1.jpg?w=558&h=429&mode=crop&scale=both"
                                    alt="" class="img-fluid">
                            </div>
                            <div>
                                <img src="<?php echo $site_path ?>/images/pro-1.jpg?w=558&h=429&mode=crop&scale=both"
                                    alt="" class="img-fluid">
                            </div>
                            <div>
                                <img src="<?php echo $site_path ?>/images/pro-1.jpg?w=558&h=429&mode=crop&scale=both"
                                    alt="" class="img-fluid">
                            </div>
                            <div>
                                <img src="<?php echo $site_path ?>/images/pro-1.jpg?w=558&h=429&mode=crop&scale=both"
                                    alt="" class="img-fluid">
                            </div>
                            <div>
                                <img src="<?php echo $site_path ?>/images/pro-1.jpg?w=558&h=429&mode=crop&scale=both"
                                    alt="" class="img-fluid">
                            </div>
                        </div>
                        <div class="product__slider__nav">
                            <div>
                                <img src="<?php echo $site_path ?>/images/pro-1.jpg?w=122&h=122&mode=crop&scale=both"
                                    alt="" class="img-fluid">
                            </div>
                            <div>
                                <img src="<?php echo $site_path ?>/images/pro-1.jpg?w=122&h=122&mode=crop&scale=both"
                                    alt="" class="img-fluid">
                            </div>
                            <div>
                                <img src="<?php echo $site_path ?>/images/pro-1.jpg?w=122&h=122&mode=crop&scale=both"
                                    alt="" class="img-fluid">
                            </div>
                            <div>
                                <img src="<?php echo $site_path ?>/images/pro-1.jpg?w=122&h=122&mode=crop&scale=both"
                                    alt="" class="img-fluid">
                            </div>
                            <div>
                                <img src="<?php echo $site_path ?>/images/pro-1.jpg?w=122&h=122&mode=crop&scale=both"
                                    alt="" class="img-fluid">
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-8 col-lg-6 col-md-6">
                        <div class="internal__header">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb justify-content-start">
                                    <li class="breadcrumb-item"><a href="<?php echo $site_url ?>/home/">Home</a>
                                    </li>
                                    <li class="breadcrumb-item " aria-current="page">
                                        <a href="<?php echo $site_url ?>/Shop/"><?php echo $current_page ?></a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">
                                        Category Name</li>
                                </ol>
                            </nav>
                            <div class="heading">
                                <h1>
                                    <?php echo $current_page ?> Product Name
                                </h1>
                            </div>
                            <div class="price mt-2 text-primary fw-bold">$150 <div
                                    class="original text-light-grey text-decoration-line-through ms-2 d-inline-block">
                                    $200</div>
                            </div>
                            <div class="select__model mt-2 d-flex align-items-center">
                                <span class="d-inline-block me-1">Model</span> <select name="" id="">
                                    <option value="">Make a choice</option>
                                    <option value="">Make a choice</option>
                                </select>
                            </div>
                            <div
                                class="cart__num mt-2 d-flex align-items-end justify-content-between flex-lg-nowrap flex-wrap">
                                <div class="d-flex align-items-center">
                                    <span class="d-inline-block me-1">Quantity</span>
                                    <div class="qtySelector">
                                        <i class="fa fa-minus decreaseQty"></i>
                                        <input type="text" class="qtyValue" value="1" />
                                        <i class="fa fa-plus increaseQty"></i>
                                    </div>
                                </div>
                                <div class="add__to mt-lg-0 mt-2">
                                    <button class="btn d-flex align-items-center">
                                        <img src="<?php echo $site_path ?>/images/cart.svg" class="me-1" alt=""> Add To
                                        Bag
                                    </button>
                                </div>
                            </div>
                            <hr class="my-2">
                            <div class="info">
                                <div class="product__code">
                                    Product Code: <span class="text-primary">LF-OB-09</span>
                                </div>
                                <div class="category">
                                    Categories: <span class="text-primary">Items</span>
                                </div>
                                <div class="category">
                                    Tags: <a href=""
                                        class="text-white d-inline-block mr-1 rounded-pill py-1 px-2 bg-primary">Gift
                                        Guide
                                        2020</a>
                                </div>
                                <div class="share">
                                    Share:
                                    <div class="social">
                                        <ul class="list-inline mb-0">
                                            <li>
                                                <a href="" target="_blank">
                                                    <i class="fab fa-facebook-f"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="" target="_blank">
                                                    <i class="fab fa-twitter"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="" target="_blank">
                                                    <i class="fab fa-instagram"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="" target="_blank">
                                                    <i class="fab fa-pinterest-p"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="details__shop__page inner__padding bg-third">
            <div class="container">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item" role="presentation">
                        <button class="nav-link active" id="details-tab" data-bs-toggle="tab" data-bs-target="#details"
                            type="button" role="tab" aria-controls="details" aria-selected="true">Product
                            Details</button>
                    </li>

                </ul>
                <div class="tab-content mt-3" id="myTabContent">
                    <div class="tab-pane fade show active" id="details" role="tabpanel" aria-labelledby="details-tab">
                        <p>This new daily glow serum combines two skin-care powerhouses, L-ascorbic acid (proven in
                            research to be the purest, best-for-skin form of the antioxidant vitamin C) and moisturizing
                            hyaluronic acid (a hydrator that’s naturally present in the body). The unique two-part
                            design maximizes potency and freshness, delivering a serum that stays active all the way
                            through its use.</p>

                        <p>Before your first use, mix the super potent 20% vitamin C powder with the hyaluronic acid
                            serum. (There are three different molecular sizes of hyaluronic acid present to penetrate
                            different layers of the skin, as well as the antioxidant superfruit Australian kakadu plum.)
                            The ingredients work synergistically to help brighten the appearance of the skin,
                            significantly improving its firmness, tone, texture, and hydration and leaving it looking
                            radiant, luminous, and healthy.</p>

                        <p>Suitable for all skin types. Apply to clean, dry face and neck in the morning and evening.
                        </p>

                        <p>Need help figuring out where to add this into your routine? Have a specific question? Send a
                            message over to <a href="" class="text-primary">beautyconcierge@goop.com</a></p>

                        <p>Caution: For external use only. Avoid contact with eyes. If irritation persists or allergic
                            reaction occurs, rinse off immediately, discontinue use, and consult a physician. Keep out
                            of reach of children.</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="topic__page inner__padding">
            <div class="container">
                <div class="title text-center">
                    <h2>Related products</h2>
                </div>
                <div class="products__items mt-3">
                    <div class="row justify-content-center">
                        <div class="col-xl-3 col-lg-4 col-md-6">
                            <div>
                                <div class="product__item">
                                    <div class="img">
                                        <img src="<?php echo $site_path; ?>/images/pro-1.jpg?w=322&h=322&mode=crop&scale=both"
                                            class="img-fluid" alt="" /><button class="add__cart border-0 p-0">
                                            <img src="<?php echo $site_path; ?>/images/cart.svg" class="svg" alt="" />
                                        </button>
                                    </div>
                                    <a href="" class="d-block">
                                        <div class="heading__price">
                                            <div class="heading">
                                                <h3>Product title goes here</h3>
                                            </div>
                                            <div class="price">
                                                <div class="original text-primary">$100</div>
                                                <div class="original text-light-grey text-decoration-line-through">
                                                    $100
                                                </div>
                                            </div>
                                        </div>
                                        <div class="category">Beauty</div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-4 col-md-6">
                            <div>
                                <div class="product__item">
                                    <div class="img">
                                        <img src="<?php echo $site_path; ?>/images/pro-1.jpg?w=322&h=322&mode=crop&scale=both"
                                            class="img-fluid" alt="" /><button class="add__cart border-0 p-0">
                                            <img src="<?php echo $site_path; ?>/images/cart.svg" class="svg" alt="" />
                                        </button>
                                    </div>
                                    <a href="" class="d-block">
                                        <div class="heading__price">
                                            <div class="heading">
                                                <h3>Product title goes here</h3>
                                            </div>
                                            <div class="price">
                                                <div class="original text-primary">$100</div>
                                                <div class="original text-light-grey text-decoration-line-through">
                                                    $100
                                                </div>
                                            </div>
                                        </div>
                                        <div class="category">Beauty</div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-4 col-md-6">
                            <div>
                                <div class="product__item">
                                    <div class="img">
                                        <img src="<?php echo $site_path; ?>/images/pro-1.jpg?w=322&h=322&mode=crop&scale=both"
                                            class="img-fluid" alt="" /><button class="add__cart border-0 p-0">
                                            <img src="<?php echo $site_path; ?>/images/cart.svg" class="svg" alt="" />
                                        </button>
                                    </div>
                                    <a href="" class="d-block">
                                        <div class="heading__price">
                                            <div class="heading">
                                                <h3>Product title goes here</h3>
                                            </div>
                                            <div class="price">
                                                <div class="original text-primary">$100</div>
                                                <div class="original text-light-grey text-decoration-line-through">
                                                    $100
                                                </div>
                                            </div>
                                        </div>
                                        <div class="category">Beauty</div>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-4 col-md-6">
                            <div>
                                <div class="product__item">
                                    <div class="img">
                                        <img src="<?php echo $site_path; ?>/images/pro-1.jpg?w=322&h=322&mode=crop&scale=both"
                                            class="img-fluid" alt="" /><button class="add__cart border-0 p-0">
                                            <img src="<?php echo $site_path; ?>/images/cart.svg" class="svg" alt="" />
                                        </button>
                                    </div>
                                    <a href="" class="d-block">
                                        <div class="heading__price">
                                            <div class="heading">
                                                <h3>Product title goes here</h3>
                                            </div>
                                            <div class="price">
                                                <div class="original text-primary">$100</div>
                                                <div class="original text-light-grey text-decoration-line-through">
                                                    $100
                                                </div>
                                            </div>
                                        </div>
                                        <div class="category">Beauty</div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include '../../component/footer.php';?>
    <?php include '../../component/scripts.php';?>
</body>

</html>