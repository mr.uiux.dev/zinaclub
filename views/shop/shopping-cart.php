<!DOCTYPE html>
<?php
$current_page ='Shopping Cart';
$timestamp = date("YmdHis");
// $site_path = 'https://www.creamyw.com/dev/zinaclub/assets';
$site_path = 'http://localhost/zinaclub/assets';
// $site_url = 'https://www.creamyw.com/dev/zinaclub/views';
$site_url = 'http://localhost/zinaclub/views';
?>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>ZinaClub | <?php echo $current_page ?></title>
    <?php include '../../component/head.php';?>
</head>

<body>
    <?php include '../../component/navbar.php';?>
    <div class="body__wraper">
        <div class="internal__header">
            <div class="container text-center">
                <div class="heading">
                    <h1>
                        <?php echo $current_page ?>
                    </h1>
                </div>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb justify-content-center">
                        <li class="breadcrumb-item"><a href="<?php echo $site_url ?>/home/">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page"> <?php echo $current_page ?></li>
                    </ol>
                </nav>
            </div>
        </div>

        <div class="cart__page inner__padding">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="d-flex align-items-center text-success">
                            <i class="fas fa-check-circle me-1"></i> One or more products were added to your cart
                        </div>
                        <div class="table-responsive-lg">
                            <table class="table mt-4">
                                <thead>
                                    <tr>
                                        <th scope="col" width="1%">Product</th>
                                        <th scope="col" width="1%">Price</th>
                                        <th scope="col" width="1%">Quantity</th>
                                        <th scope="col" width="1%">Total</th>
                                        <th scope="col" width="1%">

                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row" width="1%">
                                            <a href="" class="product__cart d-block">
                                                <div class="row align-items-center">
                                                    <div class="col-4 pe-0">
                                                        <div class="img">
                                                            <img src="<?php echo $site_path; ?>/images/pro-1.jpg?w=137&h=137&scale=both&mode=crop"
                                                                class="img-fluid" alt="" />
                                                        </div>
                                                    </div>
                                                    <div class="col-8">
                                                        <div class="category">Beauty</div>
                                                        <div class="title">Assertively fabricate top-line products</div>
                                                    </div>
                                                </div>
                                            </a>
                                        </th>
                                        <td width="1%">$120</td>
                                        <td width="1%">
                                            <div class="qtySelector">
                                                <i class="fa fa-minus decreaseQty"></i>
                                                <input type="text" class="qtyValue" value="2" />
                                                <i class="fa fa-plus increaseQty"></i>
                                            </div>
                                        </td>
                                        <td width="1%">$240</td>
                                        <td width="1%">
                                            <button class="remove"><i class="far fa-times-circle"></i></button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row" width="1%"> <a href="" class="product__cart d-block">
                                                <div class="row align-items-center">
                                                    <div class="col-4 pe-0">
                                                        <div class="img">
                                                            <img src="<?php echo $site_path; ?>/images/pro-1.jpg?w=137&h=137&scale=both&mode=crop"
                                                                class="img-fluid" alt="" />
                                                        </div>
                                                    </div>
                                                    <div class="col-8">
                                                        <div class="category">Beauty</div>
                                                        <div class="title">Assertively fabricate top-line products</div>
                                                    </div>
                                                </div>
                                            </a>
                                        </th>
                                        <td width="1%">$120</td>
                                        <td width="1%">
                                            <div class="qtySelector">
                                                <i class="fa fa-minus decreaseQty"></i>
                                                <input type="text" class="qtyValue" value="2" />
                                                <i class="fa fa-plus increaseQty"></i>
                                            </div>
                                        </td>
                                        <td width="1%">$240</td>
                                        <td width="1%">
                                            <button class="remove"><i class="far fa-times-circle"></i></button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-4 offset-xl-9 offset-lg-8 offset-md-4 col-md-8">
                        <div class=" cart__total">
                            <div class="sub__title mb-1">
                                <h3>Cart Total</h3>
                            </div>
                            <div class="subtotal mt-2 d-flex justify-content-between">
                                <span>Subtotal</span>
                                <span>$280</span>
                            </div>
                            <div class="total mt-2 d-flex justify-content-between">
                                <span>Total</span>
                                <span class="text-primary">$280</span>
                            </div>
                            <div class="tax mt-2 text-end">
                                (includes €2.71 VAT calculated for Greece)
                            </div>
                            <div class="checkout mt-2">
                                <a href="" class="btn btn-primary d-block">Proceed To Checkout</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include '../../component/footer.php';?>
        <?php include '../../component/scripts.php';?>
</body>

</html>