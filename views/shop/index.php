<!DOCTYPE html>
<?php
$current_page ='Shop';
$timestamp = date("YmdHis");
// $site_path = 'https://www.creamyw.com/dev/zinaclub/assets';
$site_path = 'http://localhost/zinaclub/assets';
// $site_url = 'https://www.creamyw.com/dev/zinaclub/views';
$site_url = 'http://localhost/zinaclub/views';
?>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>ZinaClub | <?php echo $current_page ?></title>
    <?php include '../../component/head.php';?>
</head>

<body>
    <?php include '../../component/navbar.php';?>
    <div class="body__wraper">
        <div class="internal__header">
            <div class="container text-center">
                <div class="heading">
                    <h1>
                        <?php echo $current_page ?>
                    </h1>
                </div>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb justify-content-center">
                        <li class="breadcrumb-item"><a href="<?php echo $site_url ?>/home/">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page"> <?php echo $current_page ?></li>
                    </ol>
                </nav>
            </div>
        </div>

        <div class="shop__page">
            <div class="shop__header">
                <div class="shop__bg__slider">
                    <div>
                        <div class="img">
                            <img src="<?php echo $site_path ?>/images/shop-bg.jpg?w=1440&h=550&mode=crop&scale=both"
                                class='img-fluid w-100' alt="">
                            <div class="data">
                                <div class="row w-100">
                                    <div class="offset-lg-2 col-xl-5 col-lg-6">
                                        <div class="heading" data-animation-in="fadeInUp" data-delay-in="2"
                                            data-duration-in="2">
                                            <h2>Healthy Skinis a reflection of wellness</h2>
                                        </div>
                                        <div class="link mt-3" data-animation-in="fadeInUp" data-delay-in="4"
                                            data-duration-in="2">
                                            <a href="" class="btn">Read More</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="img">
                            <img src="<?php echo $site_path ?>/images/shop-bg.jpg?w=1440&h=550&mode=crop&scale=both"
                                class='img-fluid w-100' alt="">
                            <div class="data">
                                <div class="row w-100">
                                    <div class="offset-lg-2 col-xl-5 col-lg-6">
                                        <div class="heading" data-animation-in="fadeInUp" data-delay-in="2"
                                            data-duration-in="2">
                                            <h2>Healthy Skinis a reflection of wellness</h2>
                                        </div>
                                        <div class="link mt-3" data-animation-in="fadeInUp" data-delay-in="4"
                                            data-duration-in="2">
                                            <a href="" class="btn">Read More</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="shop shop__page inner__padding">
            <div class="container">
                <div class="title">
                    <div class="d-flex justify-content-between align-items-end">
                        <h2>Popular in Beauty </h2>
                        <a href="" class="d-flex align-items-center show__all">Show All Products <i
                                class="fa fa-angle-double-right ms-1"></i></a>
                    </div>
                </div>
                <div class="product__items mt-3">
                    <div class="product__slider">
                        <div>
                            <div class="product__item">
                                <div class="img">
                                    <img src="<?php echo $site_path; ?>/images/pro-1.jpg?w=322&h=322&mode=crop&scale=both"
                                        class="img-fluid" alt="" /><button class="add__cart border-0 p-0">
                                        <img src="<?php echo $site_path; ?>/images/cart.svg" class="svg" alt="" />
                                    </button>
                                </div>
                                <a href="" class="d-block">
                                    <div class="heading__price">
                                        <div class="heading">
                                            <h3>Product title goes here</h3>
                                        </div>
                                        <div class="price">
                                            <div class="original text-primary">$100</div>
                                            <div class="original text-light-grey text-decoration-line-through">
                                                $100
                                            </div>
                                        </div>
                                    </div>
                                    <div class="category">Beauty</div>
                                </a>
                            </div>
                        </div>
                        <div>
                            <div class="product__item">
                                <div class="img">
                                    <img src="<?php echo $site_path; ?>/images/pro-1.jpg?w=322&h=322&mode=crop&scale=both"
                                        class="img-fluid" alt="" /><button class="add__cart border-0 p-0">
                                        <img src="<?php echo $site_path; ?>/images/cart.svg" class="svg" alt="" />
                                    </button>
                                </div>
                                <a href="" class="d-block">
                                    <div class="heading__price">
                                        <div class="heading">
                                            <h3>Product title goes here</h3>
                                        </div>
                                        <div class="price">
                                            <div class="original text-primary">$100</div>
                                            <div class="original text-light-grey text-decoration-line-through">
                                                $100
                                            </div>
                                        </div>
                                    </div>
                                    <div class="category">Beauty</div>
                                </a>
                            </div>
                        </div>
                        <div>
                            <div class="product__item">
                                <div class="img">
                                    <img src="<?php echo $site_path; ?>/images/pro-1.jpg?w=322&h=322&mode=crop&scale=both"
                                        class="img-fluid" alt="" /><button class="add__cart border-0 p-0">
                                        <img src="<?php echo $site_path; ?>/images/cart.svg" class="svg" alt="" />
                                    </button>
                                </div>
                                <a href="" class="d-block">
                                    <div class="heading__price">
                                        <div class="heading">
                                            <h3>Product title goes here</h3>
                                        </div>
                                        <div class="price">
                                            <div class="original text-primary">$100</div>
                                            <div class="original text-light-grey text-decoration-line-through">
                                                $100
                                            </div>
                                        </div>
                                    </div>
                                    <div class="category">Beauty</div>
                                </a>
                            </div>
                        </div>
                        <div>
                            <div class="product__item">
                                <div class="img">
                                    <img src="<?php echo $site_path; ?>/images/pro-1.jpg?w=322&h=322&mode=crop&scale=both"
                                        class="img-fluid" alt="" /><button class="add__cart border-0 p-0">
                                        <img src="<?php echo $site_path; ?>/images/cart.svg" class="svg" alt="" />
                                    </button>
                                </div>
                                <a href="" class="d-block">
                                    <div class="heading__price">
                                        <div class="heading">
                                            <h3>Product title goes here</h3>
                                        </div>
                                        <div class="price">
                                            <div class="original text-primary">$100</div>
                                            <div class="original text-light-grey text-decoration-line-through">
                                                $100
                                            </div>
                                        </div>
                                    </div>
                                    <div class="category">Beauty</div>
                                </a>
                            </div>
                        </div>
                        <div>
                            <div class="product__item">
                                <div class="img">
                                    <img src="<?php echo $site_path; ?>/images/pro-1.jpg?w=322&h=322&mode=crop&scale=both"
                                        class="img-fluid" alt="" /><button class="add__cart border-0 p-0">
                                        <img src="<?php echo $site_path; ?>/images/cart.svg" class="svg" alt="" />
                                    </button>
                                </div>
                                <a href="" class="d-block">
                                    <div class="heading__price">
                                        <div class="heading">
                                            <h3>Product title goes here</h3>
                                        </div>
                                        <div class="price">
                                            <div class="original text-primary">$100</div>
                                            <div class="original text-light-grey text-decoration-line-through">
                                                $100
                                            </div>
                                        </div>
                                    </div>
                                    <div class="category">Beauty</div>
                                </a>
                            </div>
                        </div>
                        <div>
                            <div class="product__item">
                                <div class="img">
                                    <img src="<?php echo $site_path; ?>/images/pro-1.jpg?w=322&h=322&mode=crop&scale=both"
                                        class="img-fluid" alt="" /><button class="add__cart border-0 p-0">
                                        <img src="<?php echo $site_path; ?>/images/cart.svg" class="svg" alt="" />
                                    </button>
                                </div>
                                <a href="" class="d-block">
                                    <div class="heading__price">
                                        <div class="heading">
                                            <h3>Product title goes here</h3>
                                        </div>
                                        <div class="price">
                                            <div class="original text-primary">$100</div>
                                            <div class="original text-light-grey text-decoration-line-through">
                                                $100
                                            </div>
                                        </div>
                                    </div>
                                    <div class="category">Beauty</div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="shop shop__page inner__padding bg-third">
            <div class="container">
                <div class="title">
                    <div class="d-flex justify-content-between align-items-end">
                        <h2>Popular in Book Club </h2>
                        <a href="" class="d-flex align-items-center show__all">Show All Products <i
                                class="fa fa-angle-double-right ms-1"></i></a>
                    </div>
                </div>
                <div class="product__items mt-3">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="shop__side__slider">
                                <div>
                                    <div class="product__item">
                                        <div class="img">
                                            <img src="<?php echo $site_path; ?>/images/book-bg.jpg?w=793&h=661&mode=crop&scale=both"
                                                class="img-fluid" alt="" />
                                        </div>
                                        <a href="" class="d-block">
                                            <div class="heading__price">
                                                <div class="heading">
                                                    <h3>Product title goes here</h3>
                                                </div>
                                                <div class="price">
                                                    <div class="original text-primary">$100</div>
                                                    <div class="original text-light-grey text-decoration-line-through">
                                                        $100
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="category">Beauty</div>
                                        </a>
                                    </div>
                                </div>
                                <div>
                                    <div class="product__item">
                                        <div class="img">
                                            <img src="<?php echo $site_path; ?>/images/book-bg.jpg?w=793&h=661&mode=crop&scale=both"
                                                class="img-fluid" alt="" />
                                        </div>
                                        <a href="" class="d-block">
                                            <div class="heading__price">
                                                <div class="heading">
                                                    <h3>Product title goes here</h3>
                                                </div>
                                                <div class="price">
                                                    <div class="original text-primary">$100</div>
                                                    <div class="original text-light-grey text-decoration-line-through">
                                                        $100
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="category">Beauty</div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="shop__side">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="product__item">
                                            <div class="img">
                                                <img src="<?php echo $site_path; ?>/images/pro-1.jpg?w=322&h=322&mode=crop&scale=both"
                                                    class="img-fluid" alt="" /><button class="add__cart border-0 p-0">
                                                    <img src="<?php echo $site_path; ?>/images/cart.svg" class="svg"
                                                        alt="" />
                                                </button>
                                            </div>
                                            <a href="" class="d-block">
                                                <div class="heading__price">
                                                    <div class="heading">
                                                        <h3>Product title goes here</h3>
                                                    </div>
                                                    <div class="price">
                                                        <div class="original text-primary">$100</div>
                                                        <div
                                                            class="original text-light-grey text-decoration-line-through">
                                                            $100
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="category">Beauty</div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="product__item">
                                            <div class="img">
                                                <img src="<?php echo $site_path; ?>/images/pro-1.jpg?w=322&h=322&mode=crop&scale=both"
                                                    class="img-fluid" alt="" /><button class="add__cart border-0 p-0">
                                                    <img src="<?php echo $site_path; ?>/images/cart.svg" class="svg"
                                                        alt="" />
                                                </button>
                                            </div>
                                            <a href="" class="d-block">
                                                <div class="heading__price">
                                                    <div class="heading">
                                                        <h3>Product title goes here</h3>
                                                    </div>
                                                    <div class="price">
                                                        <div class="original text-primary">$100</div>
                                                        <div
                                                            class="original text-light-grey text-decoration-line-through">
                                                            $100
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="category">Beauty</div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="product__item">
                                            <div class="img">
                                                <img src="<?php echo $site_path; ?>/images/pro-1.jpg?w=322&h=322&mode=crop&scale=both"
                                                    class="img-fluid" alt="" /><button class="add__cart border-0 p-0">
                                                    <img src="<?php echo $site_path; ?>/images/cart.svg" class="svg"
                                                        alt="" />
                                                </button>
                                            </div>
                                            <a href="" class="d-block">
                                                <div class="heading__price">
                                                    <div class="heading">
                                                        <h3>Product title goes here</h3>
                                                    </div>
                                                    <div class="price">
                                                        <div class="original text-primary">$100</div>
                                                        <div
                                                            class="original text-light-grey text-decoration-line-through">
                                                            $100
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="category">Beauty</div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="product__item">
                                            <div class="img">
                                                <img src="<?php echo $site_path; ?>/images/pro-1.jpg?w=322&h=322&mode=crop&scale=both"
                                                    class="img-fluid" alt="" /><button class="add__cart border-0 p-0">
                                                    <img src="<?php echo $site_path; ?>/images/cart.svg" class="svg"
                                                        alt="" />
                                                </button>
                                            </div>
                                            <a href="" class="d-block">
                                                <div class="heading__price">
                                                    <div class="heading">
                                                        <h3>Product title goes here</h3>
                                                    </div>
                                                    <div class="price">
                                                        <div class="original text-primary">$100</div>
                                                        <div
                                                            class="original text-light-grey text-decoration-line-through">
                                                            $100
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="category">Beauty</div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="shop shop__page inner__padding">
            <div class="container">
                <div class="title">
                    <div class="d-flex justify-content-between align-items-end">
                        <h2>Popular in Wellnes </h2>
                        <a href="" class="d-flex align-items-center show__all">Show All Products <i
                                class="fa fa-angle-double-right ms-1"></i></a>
                    </div>
                </div>
                <div class="product__items mt-3">
                    <div class="product__slider">
                        <div>
                            <div class="product__item">
                                <div class="img">
                                    <img src="<?php echo $site_path; ?>/images/pro-1.jpg?w=322&h=322&mode=crop&scale=both"
                                        class="img-fluid" alt="" /><button class="add__cart border-0 p-0">
                                        <img src="<?php echo $site_path; ?>/images/cart.svg" class="svg" alt="" />
                                    </button>
                                </div>
                                <a href="" class="d-block">
                                    <div class="heading__price">
                                        <div class="heading">
                                            <h3>Product title goes here</h3>
                                        </div>
                                        <div class="price">
                                            <div class="original text-primary">$100</div>
                                            <div class="original text-light-grey text-decoration-line-through">
                                                $100
                                            </div>
                                        </div>
                                    </div>
                                    <div class="category">Beauty</div>
                                </a>
                            </div>
                        </div>
                        <div>
                            <div class="product__item">
                                <div class="img">
                                    <img src="<?php echo $site_path; ?>/images/pro-1.jpg?w=322&h=322&mode=crop&scale=both"
                                        class="img-fluid" alt="" /><button class="add__cart border-0 p-0">
                                        <img src="<?php echo $site_path; ?>/images/cart.svg" class="svg" alt="" />
                                    </button>
                                </div>
                                <a href="" class="d-block">
                                    <div class="heading__price">
                                        <div class="heading">
                                            <h3>Product title goes here</h3>
                                        </div>
                                        <div class="price">
                                            <div class="original text-primary">$100</div>
                                            <div class="original text-light-grey text-decoration-line-through">
                                                $100
                                            </div>
                                        </div>
                                    </div>
                                    <div class="category">Beauty</div>
                                </a>
                            </div>
                        </div>
                        <div>
                            <div class="product__item">
                                <div class="img">
                                    <img src="<?php echo $site_path; ?>/images/pro-1.jpg?w=322&h=322&mode=crop&scale=both"
                                        class="img-fluid" alt="" /><button class="add__cart border-0 p-0">
                                        <img src="<?php echo $site_path; ?>/images/cart.svg" class="svg" alt="" />
                                    </button>
                                </div>
                                <a href="" class="d-block">
                                    <div class="heading__price">
                                        <div class="heading">
                                            <h3>Product title goes here</h3>
                                        </div>
                                        <div class="price">
                                            <div class="original text-primary">$100</div>
                                            <div class="original text-light-grey text-decoration-line-through">
                                                $100
                                            </div>
                                        </div>
                                    </div>
                                    <div class="category">Beauty</div>
                                </a>
                            </div>
                        </div>
                        <div>
                            <div class="product__item">
                                <div class="img">
                                    <img src="<?php echo $site_path; ?>/images/pro-1.jpg?w=322&h=322&mode=crop&scale=both"
                                        class="img-fluid" alt="" /><button class="add__cart border-0 p-0">
                                        <img src="<?php echo $site_path; ?>/images/cart.svg" class="svg" alt="" />
                                    </button>
                                </div>
                                <a href="" class="d-block">
                                    <div class="heading__price">
                                        <div class="heading">
                                            <h3>Product title goes here</h3>
                                        </div>
                                        <div class="price">
                                            <div class="original text-primary">$100</div>
                                            <div class="original text-light-grey text-decoration-line-through">
                                                $100
                                            </div>
                                        </div>
                                    </div>
                                    <div class="category">Beauty</div>
                                </a>
                            </div>
                        </div>
                        <div>
                            <div class="product__item">
                                <div class="img">
                                    <img src="<?php echo $site_path; ?>/images/pro-1.jpg?w=322&h=322&mode=crop&scale=both"
                                        class="img-fluid" alt="" /><button class="add__cart border-0 p-0">
                                        <img src="<?php echo $site_path; ?>/images/cart.svg" class="svg" alt="" />
                                    </button>
                                </div>
                                <a href="" class="d-block">
                                    <div class="heading__price">
                                        <div class="heading">
                                            <h3>Product title goes here</h3>
                                        </div>
                                        <div class="price">
                                            <div class="original text-primary">$100</div>
                                            <div class="original text-light-grey text-decoration-line-through">
                                                $100
                                            </div>
                                        </div>
                                    </div>
                                    <div class="category">Beauty</div>
                                </a>
                            </div>
                        </div>
                        <div>
                            <div class="product__item">
                                <div class="img">
                                    <img src="<?php echo $site_path; ?>/images/pro-1.jpg?w=322&h=322&mode=crop&scale=both"
                                        class="img-fluid" alt="" /><button class="add__cart border-0 p-0">
                                        <img src="<?php echo $site_path; ?>/images/cart.svg" class="svg" alt="" />
                                    </button>
                                </div>
                                <a href="" class="d-block">
                                    <div class="heading__price">
                                        <div class="heading">
                                            <h3>Product title goes here</h3>
                                        </div>
                                        <div class="price">
                                            <div class="original text-primary">$100</div>
                                            <div class="original text-light-grey text-decoration-line-through">
                                                $100
                                            </div>
                                        </div>
                                    </div>
                                    <div class="category">Beauty</div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include '../../component/footer.php';?>
    <?php include '../../component/scripts.php';?>
</body>

</html>