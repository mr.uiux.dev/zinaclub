<!DOCTYPE html>
<?php
$current_page ='Beauty';
$timestamp = date("YmdHis");
// $site_path = 'https://www.creamyw.com/dev/zinaclub/assets';
$site_path = 'http://localhost/zinaclub/assets';
// $site_url = 'https://www.creamyw.com/dev/zinaclub/views';
$site_url = 'http://localhost/zinaclub/views';
?>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>ZinaClub | <?php echo $current_page ?></title>
    <?php include '../../component/head.php';?>
</head>

<body>
    <?php include '../../component/navbar.php';?>
    <div class="body__wraper">
        <div class="internal__header">
            <div class="container text-center">
                <div class="heading">
                    <h1>
                        <?php echo $current_page ?>
                    </h1>
                </div>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb justify-content-center">
                        <li class="breadcrumb-item"><a href="<?php echo $site_url ?>/home/">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page"> <?php echo $current_page ?></li>
                    </ol>
                </nav>
            </div>
        </div>

        <div class="inner__padding topic__page">
            <div class="container">
                <div class="row">
                    <div class="col-xl-9 col-lg-8">
                        <div class="title">
                            <h2>I AM BEAUTIFUL</h2>
                        </div>
                        <div class="img mt-3">
                            <img src="<?php echo $site_path ?>/images/pic-7.jpg?w=1027&h=508&mode=crop&scale=both"
                                class=" img-fluid" alt="">
                        </div>
                        <div class="sub__title mt-2">
                            <h3>10 Natural Dry-Skin Remedies You Can DIY at Home</h3>
                        </div>
                        <div class="desc mt-2">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc at venenatis ligula. Mauris
                                sollicitudin nec leo et ornare. Nunc ac volutpat neque, in venenatis elit Lorem ipsum
                                dolor sit amet, consectetur adipiscing elit. Nunc at venenatis ligula. Mauris
                                sollicitudin nec leo et ornare. Nunc ac volutpat neque, in venenatis elit.</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc at venenatis ligula. Mauris
                                sollicitudin nec leo et ornare. Nunc ac volutpat neque, in venenatis elit Lorem ipsum
                                dolor sit amet, consectetur adipiscing elit. Nunc at venenatis ligula. Mauris
                                sollicitudin nec leo et ornare. Nunc ac volutpat neque, in venenatis elit.</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc at venenatis ligula. Mauris
                                sollicitudin nec leo et ornare. Nunc ac volutpat neque, in venenatis elit Lorem ipsum
                                dolor sit amet, consectetur adipiscing elit. Nunc at venenatis ligula. Mauris
                                sollicitudin nec leo et ornare. Nunc ac volutpat neque, in venenatis elit.</p>
                        </div>
                        <div class="date__author text-primary">
                            <span class="date">22 Dec 2021</span> -
                            <span class="author">By Author Name</span>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-4 mt-lg-0 mt-3 theia">
                        <div class="category__list">
                            <div class="heading">
                                <h3>Categories</h3>
                            </div>
                            <ul class="list-unstyled">
                                <li>
                                    <a href="">Category Name <span class="num">(5)</span></a>
                                </li>
                                <li>
                                    <a href="">Category Name <span class="num">(5)</span></a>
                                </li>
                                <li>
                                    <a href="">Category Name <span class="num">(5)</span></a>
                                </li>
                                <li>
                                    <a href="">Category Name <span class="num">(5)</span></a>
                                </li>
                            </ul>
                        </div>
                        <div class="ads text-center mt-3">
                            <img src="<?php echo $site_path ?>/images/ads.jpg?w=194&h=768&mode=crop&scale=both" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="topic__page">
            <div class="topic__slider">
                <div>
                    <div class="img">
                        <img src="<?php echo $site_path ?>/images/topic.jpg?w=1440&h=550&mode=crop&scale=both"
                            class='img-fluid w-100' alt="">
                        <div class="data text-center">
                            <div class="heading" data-animation-in="fadeInUp" data-delay-in="2" data-duration-in="2">
                                <h2>ZINA'S CHOICE</h2>
                            </div>
                            <div class="link mt-3" data-animation-in="fadeInUp" data-delay-in="4" data-duration-in="2">
                                <a href="" class="btn">Read More</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="img">
                        <img src="<?php echo $site_path ?>/images/topic.jpg?w=1440&h=550&mode=crop&scale=both"
                            class='img-fluid w-100' alt="">
                        <div class="data text-center">
                            <div class="heading" data-animation-in="fadeInUp" data-delay-in="2" data-duration-in="2">
                                <h2>ZINA'S CHOICE</h2>
                            </div>
                            <div class="link mt-3" data-animation-in="fadeInUp" data-delay-in="4" data-duration-in="2">
                                <a href="" class="btn">Read More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="shop topic__page inner__padding bg-third">
            <div class="container">
                <div class="title">
                    <div class="d-flex justify-content-between align-items-end">
                        <h2>Shop Clean Beauty</h2>
                        <a href="" class="d-flex align-items-center show__all">Show All Products <i
                                class="fa fa-angle-double-right ms-1"></i></a>
                    </div>
                </div>
                <ul class="nav nav-tabs mt-3" id="myTab" role="tablist">
                    <li class="nav-item" role="presentation">
                        <button class="nav-link active" id="pro-1-tab" data-bs-toggle="tab" data-bs-target="#pro-1"
                            type="button" role="tab" aria-controls="pro-1" aria-selected="true">Makeup</button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button class="nav-link" id="pro-2-tab" data-bs-toggle="tab" data-bs-target="#pro-2"
                            type="button" role="tab" aria-controls="pro-2" aria-selected="false">Skin Care</button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button class="nav-link" id="pro-3-tab" data-bs-toggle="tab" data-bs-target="#pro-3"
                            type="button" role="tab" aria-controls="pro-3" aria-selected="false">Bath & Body</button>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="pro-1" role="tabpanel" aria-labelledby="pro-1-tab">
                        <div class="product__items mt-3">
                            <div class="product__slider">
                                <div>
                                    <div class="product__item">
                                        <div class="img">
                                            <img src="<?php echo $site_path; ?>/images/pro-1.jpg?w=322&h=322&mode=crop&scale=both"
                                                class="img-fluid" alt="" /><button class="add__cart border-0 p-0">
                                                <img src="<?php echo $site_path; ?>/images/cart.svg" class="svg"
                                                    alt="" />
                                            </button>
                                        </div>
                                        <a href="" class="d-block">
                                            <div class="heading__price">
                                                <div class="heading">
                                                    <h3>Product title goes here</h3>
                                                </div>
                                                <div class="price">
                                                    <div class="original text-primary">$100</div>
                                                    <div class="original text-light-grey text-decoration-line-through">
                                                        $100
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="category">Beauty</div>
                                        </a>
                                    </div>
                                </div>
                                <div>
                                    <div class="product__item">
                                        <div class="img">
                                            <img src="<?php echo $site_path; ?>/images/pro-1.jpg?w=322&h=322&mode=crop&scale=both"
                                                class="img-fluid" alt="" /><button class="add__cart border-0 p-0">
                                                <img src="<?php echo $site_path; ?>/images/cart.svg" class="svg"
                                                    alt="" />
                                            </button>
                                        </div>
                                        <a href="" class="d-block">
                                            <div class="heading__price">
                                                <div class="heading">
                                                    <h3>Product title goes here</h3>
                                                </div>
                                                <div class="price">
                                                    <div class="original text-primary">$100</div>
                                                    <div class="original text-light-grey text-decoration-line-through">
                                                        $100
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="category">Beauty</div>
                                        </a>
                                    </div>
                                </div>
                                <div>
                                    <div class="product__item">
                                        <div class="img">
                                            <img src="<?php echo $site_path; ?>/images/pro-1.jpg?w=322&h=322&mode=crop&scale=both"
                                                class="img-fluid" alt="" /><button class="add__cart border-0 p-0">
                                                <img src="<?php echo $site_path; ?>/images/cart.svg" class="svg"
                                                    alt="" />
                                            </button>
                                        </div>
                                        <a href="" class="d-block">
                                            <div class="heading__price">
                                                <div class="heading">
                                                    <h3>Product title goes here</h3>
                                                </div>
                                                <div class="price">
                                                    <div class="original text-primary">$100</div>
                                                    <div class="original text-light-grey text-decoration-line-through">
                                                        $100
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="category">Beauty</div>
                                        </a>
                                    </div>
                                </div>
                                <div>
                                    <div class="product__item">
                                        <div class="img">
                                            <img src="<?php echo $site_path; ?>/images/pro-1.jpg?w=322&h=322&mode=crop&scale=both"
                                                class="img-fluid" alt="" /><button class="add__cart border-0 p-0">
                                                <img src="<?php echo $site_path; ?>/images/cart.svg" class="svg"
                                                    alt="" />
                                            </button>
                                        </div>
                                        <a href="" class="d-block">
                                            <div class="heading__price">
                                                <div class="heading">
                                                    <h3>Product title goes here</h3>
                                                </div>
                                                <div class="price">
                                                    <div class="original text-primary">$100</div>
                                                    <div class="original text-light-grey text-decoration-line-through">
                                                        $100
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="category">Beauty</div>
                                        </a>
                                    </div>
                                </div>
                                <div>
                                    <div class="product__item">
                                        <div class="img">
                                            <img src="<?php echo $site_path; ?>/images/pro-1.jpg?w=322&h=322&mode=crop&scale=both"
                                                class="img-fluid" alt="" /><button class="add__cart border-0 p-0">
                                                <img src="<?php echo $site_path; ?>/images/cart.svg" class="svg"
                                                    alt="" />
                                            </button>
                                        </div>
                                        <a href="" class="d-block">
                                            <div class="heading__price">
                                                <div class="heading">
                                                    <h3>Product title goes here</h3>
                                                </div>
                                                <div class="price">
                                                    <div class="original text-primary">$100</div>
                                                    <div class="original text-light-grey text-decoration-line-through">
                                                        $100
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="category">Beauty</div>
                                        </a>
                                    </div>
                                </div>
                                <div>
                                    <div class="product__item">
                                        <div class="img">
                                            <img src="<?php echo $site_path; ?>/images/pro-1.jpg?w=322&h=322&mode=crop&scale=both"
                                                class="img-fluid" alt="" /><button class="add__cart border-0 p-0">
                                                <img src="<?php echo $site_path; ?>/images/cart.svg" class="svg"
                                                    alt="" />
                                            </button>
                                        </div>
                                        <a href="" class="d-block">
                                            <div class="heading__price">
                                                <div class="heading">
                                                    <h3>Product title goes here</h3>
                                                </div>
                                                <div class="price">
                                                    <div class="original text-primary">$100</div>
                                                    <div class="original text-light-grey text-decoration-line-through">
                                                        $100
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="category">Beauty</div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="pro-2" role="tabpanel" aria-labelledby="pro-2-tab">
                        <div class="product__items mt-3">
                            <div class="product__slider">
                                <div>
                                    <div class="product__item">
                                        <div class="img">
                                            <img src="<?php echo $site_path; ?>/images/pro-1.jpg?w=322&h=322&mode=crop&scale=both"
                                                class="img-fluid" alt="" /><button class="add__cart border-0 p-0">
                                                <img src="<?php echo $site_path; ?>/images/cart.svg" class="svg"
                                                    alt="" />
                                            </button>
                                        </div>
                                        <a href="" class="d-block">
                                            <div class="heading__price">
                                                <div class="heading">
                                                    <h3>Product title goes here</h3>
                                                </div>
                                                <div class="price">
                                                    <div class="original text-primary">$100</div>
                                                    <div class="original text-light-grey text-decoration-line-through">
                                                        $100
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="category">Beauty</div>
                                        </a>
                                    </div>
                                </div>
                                <div>
                                    <div class="product__item">
                                        <div class="img">
                                            <img src="<?php echo $site_path; ?>/images/pro-1.jpg?w=322&h=322&mode=crop&scale=both"
                                                class="img-fluid" alt="" /><button class="add__cart border-0 p-0">
                                                <img src="<?php echo $site_path; ?>/images/cart.svg" class="svg"
                                                    alt="" />
                                            </button>
                                        </div>
                                        <a href="" class="d-block">
                                            <div class="heading__price">
                                                <div class="heading">
                                                    <h3>Product title goes here</h3>
                                                </div>
                                                <div class="price">
                                                    <div class="original text-primary">$100</div>
                                                    <div class="original text-light-grey text-decoration-line-through">
                                                        $100
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="category">Beauty</div>
                                        </a>
                                    </div>
                                </div>
                                <div>
                                    <div class="product__item">
                                        <div class="img">
                                            <img src="<?php echo $site_path; ?>/images/pro-1.jpg?w=322&h=322&mode=crop&scale=both"
                                                class="img-fluid" alt="" /><button class="add__cart border-0 p-0">
                                                <img src="<?php echo $site_path; ?>/images/cart.svg" class="svg"
                                                    alt="" />
                                            </button>
                                        </div>
                                        <a href="" class="d-block">
                                            <div class="heading__price">
                                                <div class="heading">
                                                    <h3>Product title goes here</h3>
                                                </div>
                                                <div class="price">
                                                    <div class="original text-primary">$100</div>
                                                    <div class="original text-light-grey text-decoration-line-through">
                                                        $100
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="category">Beauty</div>
                                        </a>
                                    </div>
                                </div>
                                <div>
                                    <div class="product__item">
                                        <div class="img">
                                            <img src="<?php echo $site_path; ?>/images/pro-1.jpg?w=322&h=322&mode=crop&scale=both"
                                                class="img-fluid" alt="" /><button class="add__cart border-0 p-0">
                                                <img src="<?php echo $site_path; ?>/images/cart.svg" class="svg"
                                                    alt="" />
                                            </button>
                                        </div>
                                        <a href="" class="d-block">
                                            <div class="heading__price">
                                                <div class="heading">
                                                    <h3>Product title goes here</h3>
                                                </div>
                                                <div class="price">
                                                    <div class="original text-primary">$100</div>
                                                    <div class="original text-light-grey text-decoration-line-through">
                                                        $100
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="category">Beauty</div>
                                        </a>
                                    </div>
                                </div>
                                <div>
                                    <div class="product__item">
                                        <div class="img">
                                            <img src="<?php echo $site_path; ?>/images/pro-1.jpg?w=322&h=322&mode=crop&scale=both"
                                                class="img-fluid" alt="" /><button class="add__cart border-0 p-0">
                                                <img src="<?php echo $site_path; ?>/images/cart.svg" class="svg"
                                                    alt="" />
                                            </button>
                                        </div>
                                        <a href="" class="d-block">
                                            <div class="heading__price">
                                                <div class="heading">
                                                    <h3>Product title goes here</h3>
                                                </div>
                                                <div class="price">
                                                    <div class="original text-primary">$100</div>
                                                    <div class="original text-light-grey text-decoration-line-through">
                                                        $100
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="category">Beauty</div>
                                        </a>
                                    </div>
                                </div>
                                <div>
                                    <div class="product__item">
                                        <div class="img">
                                            <img src="<?php echo $site_path; ?>/images/pro-1.jpg?w=322&h=322&mode=crop&scale=both"
                                                class="img-fluid" alt="" /><button class="add__cart border-0 p-0">
                                                <img src="<?php echo $site_path; ?>/images/cart.svg" class="svg"
                                                    alt="" />
                                            </button>
                                        </div>
                                        <a href="" class="d-block">
                                            <div class="heading__price">
                                                <div class="heading">
                                                    <h3>Product title goes here</h3>
                                                </div>
                                                <div class="price">
                                                    <div class="original text-primary">$100</div>
                                                    <div class="original text-light-grey text-decoration-line-through">
                                                        $100
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="category">Beauty</div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="pro-3" role="tabpanel" aria-labelledby="pro-3-tab">
                        <div class="product__items mt-3">
                            <div class="product__slider">
                                <div>
                                    <div class="product__item">
                                        <div class="img">
                                            <img src="<?php echo $site_path; ?>/images/pro-1.jpg?w=322&h=322&mode=crop&scale=both"
                                                class="img-fluid" alt="" /><button class="add__cart border-0 p-0">
                                                <img src="<?php echo $site_path; ?>/images/cart.svg" class="svg"
                                                    alt="" />
                                            </button>
                                        </div>
                                        <a href="" class="d-block">
                                            <div class="heading__price">
                                                <div class="heading">
                                                    <h3>Product title goes here</h3>
                                                </div>
                                                <div class="price">
                                                    <div class="original text-primary">$100</div>
                                                    <div class="original text-light-grey text-decoration-line-through">
                                                        $100
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="category">Beauty</div>
                                        </a>
                                    </div>
                                </div>
                                <div>
                                    <div class="product__item">
                                        <div class="img">
                                            <img src="<?php echo $site_path; ?>/images/pro-1.jpg?w=322&h=322&mode=crop&scale=both"
                                                class="img-fluid" alt="" /><button class="add__cart border-0 p-0">
                                                <img src="<?php echo $site_path; ?>/images/cart.svg" class="svg"
                                                    alt="" />
                                            </button>
                                        </div>
                                        <a href="" class="d-block">
                                            <div class="heading__price">
                                                <div class="heading">
                                                    <h3>Product title goes here</h3>
                                                </div>
                                                <div class="price">
                                                    <div class="original text-primary">$100</div>
                                                    <div class="original text-light-grey text-decoration-line-through">
                                                        $100
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="category">Beauty</div>
                                        </a>
                                    </div>
                                </div>
                                <div>
                                    <div class="product__item">
                                        <div class="img">
                                            <img src="<?php echo $site_path; ?>/images/pro-1.jpg?w=322&h=322&mode=crop&scale=both"
                                                class="img-fluid" alt="" /><button class="add__cart border-0 p-0">
                                                <img src="<?php echo $site_path; ?>/images/cart.svg" class="svg"
                                                    alt="" />
                                            </button>
                                        </div>
                                        <a href="" class="d-block">
                                            <div class="heading__price">
                                                <div class="heading">
                                                    <h3>Product title goes here</h3>
                                                </div>
                                                <div class="price">
                                                    <div class="original text-primary">$100</div>
                                                    <div class="original text-light-grey text-decoration-line-through">
                                                        $100
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="category">Beauty</div>
                                        </a>
                                    </div>
                                </div>
                                <div>
                                    <div class="product__item">
                                        <div class="img">
                                            <img src="<?php echo $site_path; ?>/images/pro-1.jpg?w=322&h=322&mode=crop&scale=both"
                                                class="img-fluid" alt="" /><button class="add__cart border-0 p-0">
                                                <img src="<?php echo $site_path; ?>/images/cart.svg" class="svg"
                                                    alt="" />
                                            </button>
                                        </div>
                                        <a href="" class="d-block">
                                            <div class="heading__price">
                                                <div class="heading">
                                                    <h3>Product title goes here</h3>
                                                </div>
                                                <div class="price">
                                                    <div class="original text-primary">$100</div>
                                                    <div class="original text-light-grey text-decoration-line-through">
                                                        $100
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="category">Beauty</div>
                                        </a>
                                    </div>
                                </div>
                                <div>
                                    <div class="product__item">
                                        <div class="img">
                                            <img src="<?php echo $site_path; ?>/images/pro-1.jpg?w=322&h=322&mode=crop&scale=both"
                                                class="img-fluid" alt="" /><button class="add__cart border-0 p-0">
                                                <img src="<?php echo $site_path; ?>/images/cart.svg" class="svg"
                                                    alt="" />
                                            </button>
                                        </div>
                                        <a href="" class="d-block">
                                            <div class="heading__price">
                                                <div class="heading">
                                                    <h3>Product title goes here</h3>
                                                </div>
                                                <div class="price">
                                                    <div class="original text-primary">$100</div>
                                                    <div class="original text-light-grey text-decoration-line-through">
                                                        $100
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="category">Beauty</div>
                                        </a>
                                    </div>
                                </div>
                                <div>
                                    <div class="product__item">
                                        <div class="img">
                                            <img src="<?php echo $site_path; ?>/images/pro-1.jpg?w=322&h=322&mode=crop&scale=both"
                                                class="img-fluid" alt="" /><button class="add__cart border-0 p-0">
                                                <img src="<?php echo $site_path; ?>/images/cart.svg" class="svg"
                                                    alt="" />
                                            </button>
                                        </div>
                                        <a href="" class="d-block">
                                            <div class="heading__price">
                                                <div class="heading">
                                                    <h3>Product title goes here</h3>
                                                </div>
                                                <div class="price">
                                                    <div class="original text-primary">$100</div>
                                                    <div class="original text-light-grey text-decoration-line-through">
                                                        $100
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="category">Beauty</div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="inner__padding topic__page">
            <div class="container">
                <div class="row">
                    <div class="col-xl-8 col-lg-9">
                        <div class="title">
                            <div class="d-flex justify-content-between align-items-end">
                                <h2>Makeup</h2>
                                <a href="" class="d-flex align-items-center show__all">Show All Products <i
                                        class="fa fa-angle-double-right ms-1"></i></a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-8 col-md-6 order-lg-0 order-md-0">
                                <a href="" class="beauty__item bg__item d-block">
                                    <div class="img">
                                        <img src="<?php echo $site_path; ?>/images/pic-1.jpg?w=1381&h=464&mode=crop&scale=both"
                                            class="img-fluid" alt="" />
                                        <div class="data">
                                            <div class="heading">
                                                <h3>Nine Tips to Keep Your Skin Glowing</h3>
                                            </div>
                                            <div class="desc">
                                                <p>
                                                    Winter doesn't have to mean dry skin! Indeed, these at-home
                                                    remedies can help soothe even the most irritated, cracked
                                                </p>
                                            </div>
                                            <div class="date__author fw-lighter text-end">
                                                <span class="date">22 Dec 2021</span> -
                                                <span class="author">By Author Name</span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-4 col-md-6 order-lg-1 order-md-2">
                                <a href="" class="beauty__item top__item d-block">
                                    <div class="img">
                                        <img src="<?php echo $site_path; ?>/images/pic-3.jpg?w=440&h=281&mode=crop&scale=both"
                                            class="img-fluid" alt="" />
                                    </div>
                                    <div class="data">
                                        <div class="heading">
                                            <h3>10 Natural Dry-Skin Remedies You Can DIY at Home</h3>
                                        </div>
                                        <div class="desc">
                                            <p>
                                                Winter doesn't have to mean dry skin! Indeed, these at-home
                                                remedies can help soothe even the most irritated, cracked
                                            </p>
                                        </div>
                                        <div class="date__author fw-lighter text-primary">
                                            <span class="date">22 Dec 2021</span> -
                                            <span class="author">By Author Name</span>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-4 col-md-6 order-lg-2 order-md-3">
                                <a href="" class="beauty__item top__item d-block">
                                    <div class="img">
                                        <img src="<?php echo $site_path; ?>/images/pic-3.jpg?w=440&h=281&mode=crop&scale=both"
                                            class="img-fluid" alt="" />
                                    </div>
                                    <div class="data">
                                        <div class="heading">
                                            <h3>10 Natural Dry-Skin Remedies You Can DIY at Home</h3>
                                        </div>
                                        <div class="desc">
                                            <p>
                                                Winter doesn't have to mean dry skin! Indeed, these at-home
                                                remedies can help soothe even the most irritated, cracked
                                            </p>
                                        </div>
                                        <div class="date__author fw-lighter text-primary">
                                            <span class="date">22 Dec 2021</span> -
                                            <span class="author">By Author Name</span>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-8 col-md-6 order-lg-3 order-md-1">
                                <a href="" class="beauty__item bg__item d-block">
                                    <div class="img">
                                        <img src="<?php echo $site_path; ?>/images/pic-1.jpg?w=1381&h=464&mode=crop&scale=both"
                                            class="img-fluid" alt="" />
                                        <div class="data">
                                            <div class="heading">
                                                <h3>Nine Tips to Keep Your Skin Glowing</h3>
                                            </div>
                                            <div class="desc">
                                                <p>
                                                    Winter doesn't have to mean dry skin! Indeed, these at-home
                                                    remedies can help soothe even the most irritated, cracked
                                                </p>
                                            </div>
                                            <div class="date__author fw-lighter text-end">
                                                <span class="date">22 Dec 2021</span> -
                                                <span class="author">By Author Name</span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-3 mt-lg-0 mt-3">
                        <div class="title">
                            <div class="d-flex justify-content-between align-items-end">
                                <h2>Skin Care</h2>
                                <a href="" class="d-flex align-items-center show__all">Show All Products <i
                                        class="fa fa-angle-double-right ms-1"></i></a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-6">
                                <a href="" class="beauty__item side__item d-block">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="img">
                                                <img src="<?php echo $site_path; ?>/images/pic-2.jpg?w=235&h=235&mode=crop&scale=both"
                                                    class="img-fluid" alt="" />
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="data">
                                                <div class="heading">
                                                    <h3>10 Natural Dry-Skin Remedies You Can DIY at Home</h3>
                                                </div>
                                                <div class="desc">
                                                    <p>
                                                        Winter doesn't have to mean dry skin! Indeed, these at-home
                                                        remedies can help soothe even the most irritated, cracked
                                                    </p>
                                                </div>
                                                <div class="date__author fw-lighter text-primary">
                                                    <span class="date">22 Dec 2021</span> -
                                                    <span class="author">By Author Name</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-12 col-md-6">
                                <a href="" class="beauty__item side__item d-block">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="img">
                                                <img src="<?php echo $site_path; ?>/images/pic-2.jpg?w=235&h=235&mode=crop&scale=both"
                                                    class="img-fluid" alt="" />
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="data">
                                                <div class="heading">
                                                    <h3>10 Natural Dry-Skin Remedies You Can DIY at Home</h3>
                                                </div>
                                                <div class="desc">
                                                    <p>
                                                        Winter doesn't have to mean dry skin! Indeed, these at-home
                                                        remedies can help soothe even the most irritated, cracked
                                                    </p>
                                                </div>
                                                <div class="date__author fw-lighter text-primary">
                                                    <span class="date">22 Dec 2021</span> -
                                                    <span class="author">By Author Name</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-12 col-md-6">
                                <a href="" class="beauty__item side__item d-block">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="img">
                                                <img src="<?php echo $site_path; ?>/images/pic-2.jpg?w=235&h=235&mode=crop&scale=both"
                                                    class="img-fluid" alt="" />
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="data">
                                                <div class="heading">
                                                    <h3>10 Natural Dry-Skin Remedies You Can DIY at Home</h3>
                                                </div>
                                                <div class="desc">
                                                    <p>
                                                        Winter doesn't have to mean dry skin! Indeed, these at-home
                                                        remedies can help soothe even the most irritated, cracked
                                                    </p>
                                                </div>
                                                <div class="date__author fw-lighter text-primary">
                                                    <span class="date">22 Dec 2021</span> -
                                                    <span class="author">By Author Name</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-12 col-md-6">
                                <a href="" class="beauty__item side__item d-block">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="img">
                                                <img src="<?php echo $site_path; ?>/images/pic-2.jpg?w=235&h=235&mode=crop&scale=both"
                                                    class="img-fluid" alt="" />
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="data">
                                                <div class="heading">
                                                    <h3>10 Natural Dry-Skin Remedies You Can DIY at Home</h3>
                                                </div>
                                                <div class="desc">
                                                    <p>
                                                        Winter doesn't have to mean dry skin! Indeed, these at-home
                                                        remedies can help soothe even the most irritated, cracked
                                                    </p>
                                                </div>
                                                <div class="date__author fw-lighter text-primary">
                                                    <span class="date">22 Dec 2021</span> -
                                                    <span class="author">By Author Name</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="inner__padding topic__page bg-third">
            <div class="container">
                <div class="title">
                    <div class="d-flex justify-content-between align-items-end">
                        <h2>Bath & Body</h2>
                        <a href="" class="d-flex align-items-center show__all">Show All Products <i
                                class="fa fa-angle-double-right ms-1"></i></a>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-12">
                        <a href="" class="beauty__item bg__item d-block">
                            <div class="img">
                                <img src="<?php echo $site_path; ?>/images/pic-1.jpg?w=1381&h=464&mode=crop&scale=both"
                                    class="img-fluid" alt="" />
                                <div class="data">
                                    <div class="heading">
                                        <h3>Nine Tips to Keep Your Skin Glowing</h3>
                                    </div>
                                    <div class="desc">
                                        <p>
                                            Winter doesn't have to mean dry skin! Indeed, these at-home
                                            remedies can help soothe even the most irritated, cracked
                                        </p>
                                    </div>
                                    <div class="date__author fw-lighter text-end">
                                        <span class="date">22 Dec 2021</span> -
                                        <span class="author">By Author Name</span>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-12 mt-lg-3 mt-0">
                        <div class="row justify-content-center">
                            <div class="col-lg-4 col-md-6">
                                <a href="" class="beauty__item top__item d-block">
                                    <div class="img">
                                        <img src="<?php echo $site_path; ?>/images/pic-3.jpg?w=440&h=281&mode=crop&scale=both"
                                            class="img-fluid" alt="" />
                                    </div>
                                    <div class="data">
                                        <div class="heading">
                                            <h3>10 Natural Dry-Skin Remedies You Can DIY at Home</h3>
                                        </div>
                                        <div class="desc">
                                            <p>
                                                Winter doesn't have to mean dry skin! Indeed, these at-home
                                                remedies can help soothe even the most irritated, cracked
                                            </p>
                                        </div>
                                        <div class="date__author fw-lighter text-primary">
                                            <span class="date">22 Dec 2021</span> -
                                            <span class="author">By Author Name</span>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-4 col-md-6">
                                <a href="" class="beauty__item top__item d-block">
                                    <div class="img">
                                        <img src="<?php echo $site_path; ?>/images/pic-3.jpg?w=440&h=281&mode=crop&scale=both"
                                            class="img-fluid" alt="" />
                                    </div>
                                    <div class="data">
                                        <div class="heading">
                                            <h3>10 Natural Dry-Skin Remedies You Can DIY at Home</h3>
                                        </div>
                                        <div class="desc">
                                            <p>
                                                Winter doesn't have to mean dry skin! Indeed, these at-home
                                                remedies can help soothe even the most irritated, cracked
                                            </p>
                                        </div>
                                        <div class="date__author fw-lighter text-primary">
                                            <span class="date">22 Dec 2021</span> -
                                            <span class="author">By Author Name</span>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-4 col-md-6">
                                <a href="" class="beauty__item top__item d-block">
                                    <div class="img">
                                        <img src="<?php echo $site_path; ?>/images/pic-3.jpg?w=440&h=281&mode=crop&scale=both"
                                            class="img-fluid" alt="" />
                                    </div>
                                    <div class="data">
                                        <div class="heading">
                                            <h3>10 Natural Dry-Skin Remedies You Can DIY at Home</h3>
                                        </div>
                                        <div class="desc">
                                            <p>
                                                Winter doesn't have to mean dry skin! Indeed, these at-home
                                                remedies can help soothe even the most irritated, cracked
                                            </p>
                                        </div>
                                        <div class="date__author fw-lighter text-primary">
                                            <span class="date">22 Dec 2021</span> -
                                            <span class="author">By Author Name</span>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include '../../component/footer.php';?>
    <?php include '../../component/scripts.php';?>
</body>

</html>