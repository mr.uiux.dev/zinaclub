<!DOCTYPE html>
<?php
$current_page ='Beauty';
$timestamp = date("YmdHis");
// $site_path = 'https://www.creamyw.com/dev/zinaclub/assets';
$site_path = 'http://localhost/zinaclub/assets';
// $site_url = 'https://www.creamyw.com/dev/zinaclub/views';
$site_url = 'http://localhost/zinaclub/views';
?>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>ZinaClub | <?php echo $current_page ?></title>
    <?php include '../../component/head.php';?>
</head>

<body>
    <?php include '../../component/navbar.php';?>
    <div class="body__wraper">
        <div class="internal__header">
            <div class="container text-center">
                <div class="heading">
                    <h1>
                        <?php echo $current_page ?>
                    </h1>
                </div>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb justify-content-center">
                        <li class="breadcrumb-item"><a href="<?php echo $site_url ?>/home/">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page"> <?php echo $current_page ?></li>
                    </ol>
                </nav>
            </div>
        </div>

        <div class="bg__banner">
            <div class="container bg-third p-0">
                <div class="row g-0">
                    <div class="col-md-6">
                        <div class="img">
                            <img src="<?php echo $site_path ?>/images/pic-7.jpg?w=1027&h=508&mode=crop&scale=both"
                                class=" img-fluid" alt="">
                        </div>
                    </div>
                    <div class="col-md-6 align-self-center ps-3 py-md-0 py-3">
                        <div class="heading">
                            <h2>I AM BEAUTIFUL</h2>
                        </div>

                        <div class="date__author mt-2">
                            <span class="date">22 Dec 2021</span> -
                            <span class="author">By Author Name</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="inner__padding topic__page">
            <div class="container">
                <div class="row">
                    <div class="col-xl-8 col-lg-7">
                        <div class="sub__title">
                            <h3>10 Natural Dry-Skin Remedies You Can DIY at Home</h3>
                        </div>
                        <div class="desc mt-2">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc at venenatis ligula. Mauris
                                sollicitudin nec leo et ornare. Nunc ac volutpat neque, in venenatis elit Lorem ipsum
                                dolor sit amet, consectetur adipiscing elit. Nunc at venenatis ligula. Mauris
                                sollicitudin nec leo et ornare. Nunc ac volutpat neque, in venenatis elit.</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc at venenatis ligula. Mauris
                                sollicitudin nec leo et ornare. Nunc ac volutpat neque, in venenatis elit Lorem ipsum
                                dolor sit amet, consectetur adipiscing elit. Nunc at venenatis ligula. Mauris
                                sollicitudin nec leo et ornare. Nunc ac volutpat neque, in venenatis elit.</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc at venenatis ligula. Mauris
                                sollicitudin nec leo et ornare. Nunc ac volutpat neque, in venenatis elit Lorem ipsum
                                dolor sit amet, consectetur adipiscing elit. Nunc at venenatis ligula. Mauris
                                sollicitudin nec leo et ornare. Nunc ac volutpat neque, in venenatis elit.</p>
                        </div>
                        <div class="img mt-2">
                            <img src="<?php echo $site_path ?>/images/pic-7.jpg?w=1027&h=508&mode=crop&scale=both"
                                class=" img-fluid" alt="">
                        </div>
                        <div class="sub__title mt-2">
                            <h3>10 Natural Dry-Skin Remedies You Can DIY at Home</h3>
                        </div>
                        <div class="desc mt-2">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc at venenatis ligula. Mauris
                                sollicitudin nec leo et ornare. Nunc ac volutpat neque, in venenatis elit Lorem ipsum
                                dolor sit amet, consectetur adipiscing elit. Nunc at venenatis ligula. Mauris
                                sollicitudin nec leo et ornare. Nunc ac volutpat neque, in venenatis elit.</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc at venenatis ligula. Mauris
                                sollicitudin nec leo et ornare. Nunc ac volutpat neque, in venenatis elit Lorem ipsum
                                dolor sit amet, consectetur adipiscing elit. Nunc at venenatis ligula. Mauris
                                sollicitudin nec leo et ornare. Nunc ac volutpat neque, in venenatis elit.</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc at venenatis ligula. Mauris
                                sollicitudin nec leo et ornare. Nunc ac volutpat neque, in venenatis elit Lorem ipsum
                                dolor sit amet, consectetur adipiscing elit. Nunc at venenatis ligula. Mauris
                                sollicitudin nec leo et ornare. Nunc ac volutpat neque, in venenatis elit.</p>
                        </div>
                        <div class="share mt-3">
                            Share:
                            <div class="social">
                                <ul class="list-inline mb-0">
                                    <li>
                                        <a href="" target="_blank">
                                            <i class="fab fa-facebook-f"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="" target="_blank">
                                            <i class="fab fa-twitter"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="" target="_blank">
                                            <i class="fab fa-youtube"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="" target="_blank">
                                            <i class="fab fa-instagram"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="" target="_blank">
                                            <i class="fab fa-pinterest-p"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-5 mt-lg-0 mt-3 theia">
                        <div class="title">
                            <div class="d-flex justify-content-between align-items-end">
                                <h2>Skin Care</h2>
                                <a href="" class="d-flex align-items-center show__all">Show All Products <i
                                        class="fa fa-angle-double-right ms-1"></i></a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-6">
                                <a href="" class="beauty__item side__item d-block">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="img">
                                                <img src="<?php echo $site_path; ?>/images/pic-2.jpg?w=235&h=235&mode=crop&scale=both"
                                                    class="img-fluid" alt="" />
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="data">
                                                <div class="heading">
                                                    <h3>10 Natural Dry-Skin Remedies You Can DIY at Home</h3>
                                                </div>
                                                <div class="desc">
                                                    <p>
                                                        Winter doesn't have to mean dry skin! Indeed, these at-home
                                                        remedies can help soothe even the most irritated, cracked
                                                    </p>
                                                </div>
                                                <div class="date__author fw-lighter text-primary">
                                                    <span class="date">22 Dec 2021</span> -
                                                    <span class="author">By Author Name</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-12 col-md-6">
                                <a href="" class="beauty__item side__item d-block">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="img">
                                                <img src="<?php echo $site_path; ?>/images/pic-2.jpg?w=235&h=235&mode=crop&scale=both"
                                                    class="img-fluid" alt="" />
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="data">
                                                <div class="heading">
                                                    <h3>10 Natural Dry-Skin Remedies You Can DIY at Home</h3>
                                                </div>
                                                <div class="desc">
                                                    <p>
                                                        Winter doesn't have to mean dry skin! Indeed, these at-home
                                                        remedies can help soothe even the most irritated, cracked
                                                    </p>
                                                </div>
                                                <div class="date__author fw-lighter text-primary">
                                                    <span class="date">22 Dec 2021</span> -
                                                    <span class="author">By Author Name</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-12 col-md-6">
                                <a href="" class="beauty__item side__item d-block">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="img">
                                                <img src="<?php echo $site_path; ?>/images/pic-2.jpg?w=235&h=235&mode=crop&scale=both"
                                                    class="img-fluid" alt="" />
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="data">
                                                <div class="heading">
                                                    <h3>10 Natural Dry-Skin Remedies You Can DIY at Home</h3>
                                                </div>
                                                <div class="desc">
                                                    <p>
                                                        Winter doesn't have to mean dry skin! Indeed, these at-home
                                                        remedies can help soothe even the most irritated, cracked
                                                    </p>
                                                </div>
                                                <div class="date__author fw-lighter text-primary">
                                                    <span class="date">22 Dec 2021</span> -
                                                    <span class="author">By Author Name</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-12 col-md-6">
                                <a href="" class="beauty__item side__item d-block">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="img">
                                                <img src="<?php echo $site_path; ?>/images/pic-2.jpg?w=235&h=235&mode=crop&scale=both"
                                                    class="img-fluid" alt="" />
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="data">
                                                <div class="heading">
                                                    <h3>10 Natural Dry-Skin Remedies You Can DIY at Home</h3>
                                                </div>
                                                <div class="desc">
                                                    <p>
                                                        Winter doesn't have to mean dry skin! Indeed, these at-home
                                                        remedies can help soothe even the most irritated, cracked
                                                    </p>
                                                </div>
                                                <div class="date__author fw-lighter text-primary">
                                                    <span class="date">22 Dec 2021</span> -
                                                    <span class="author">By Author Name</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="topic__page inner__padding bg-third">
            <div class="container">
                <div class="title text-center">
                    <h2>ΠΕΡΙΣΣΟΤΕΡΑ ΑΡΘΡΑ ΑΠΟ ΤΗΝ ΚΑΤΗΓΟΡΙΑ ΑΠΟΨΗ</h2>
                </div>
                <div class="wellness__items mt-3">
                    <div class="row justify-content-center">
                        <div class="col-xl-3 col-lg-4 col-md-6">
                            <a href="" class="wellness__item top__item d-block">
                                <div class="img">
                                    <img src="<?php echo $site_path; ?>/images/pic-3.jpg?w=440&h=281&mode=crop&scale=both"
                                        class="img-fluid" alt="" />
                                </div>
                                <div class="data">
                                    <div class="heading">
                                        <h3>10 Natural Dry-Skin Remedies You Can DIY at Home</h3>
                                    </div>
                                    <div class="desc">
                                        <p>
                                            Winter doesn't have to mean dry skin! Indeed, these at-home
                                            remedies can help soothe even the most irritated, cracked
                                        </p>
                                    </div>
                                    <div class="date__author fw-lighter text-primary">
                                        <span class="date">22 Dec 2021</span> -
                                        <span class="author">By Author Name</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-xl-3 col-lg-4 col-md-6">
                            <a href="" class="wellness__item top__item d-block">
                                <div class="img">
                                    <img src="<?php echo $site_path; ?>/images/pic-3.jpg?w=440&h=281&mode=crop&scale=both"
                                        class="img-fluid" alt="" />
                                </div>
                                <div class="data">
                                    <div class="heading">
                                        <h3>10 Natural Dry-Skin Remedies You Can DIY at Home</h3>
                                    </div>
                                    <div class="desc">
                                        <p>
                                            Winter doesn't have to mean dry skin! Indeed, these at-home
                                            remedies can help soothe even the most irritated, cracked
                                        </p>
                                    </div>
                                    <div class="date__author fw-lighter text-primary">
                                        <span class="date">22 Dec 2021</span> -
                                        <span class="author">By Author Name</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-xl-3 col-lg-4 col-md-6">
                            <a href="" class="wellness__item top__item d-block">
                                <div class="img">
                                    <img src="<?php echo $site_path; ?>/images/pic-3.jpg?w=440&h=281&mode=crop&scale=both"
                                        class="img-fluid" alt="" />
                                </div>
                                <div class="data">
                                    <div class="heading">
                                        <h3>10 Natural Dry-Skin Remedies You Can DIY at Home</h3>
                                    </div>
                                    <div class="desc">
                                        <p>
                                            Winter doesn't have to mean dry skin! Indeed, these at-home
                                            remedies can help soothe even the most irritated, cracked
                                        </p>
                                    </div>
                                    <div class="date__author fw-lighter text-primary">
                                        <span class="date">22 Dec 2021</span> -
                                        <span class="author">By Author Name</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-xl-3 col-lg-4 col-md-6">
                            <a href="" class="wellness__item top__item d-block">
                                <div class="img">
                                    <img src="<?php echo $site_path; ?>/images/pic-3.jpg?w=440&h=281&mode=crop&scale=both"
                                        class="img-fluid" alt="" />
                                </div>
                                <div class="data">
                                    <div class="heading">
                                        <h3>10 Natural Dry-Skin Remedies You Can DIY at Home</h3>
                                    </div>
                                    <div class="desc">
                                        <p>
                                            Winter doesn't have to mean dry skin! Indeed, these at-home
                                            remedies can help soothe even the most irritated, cracked
                                        </p>
                                    </div>
                                    <div class="date__author fw-lighter text-primary">
                                        <span class="date">22 Dec 2021</span> -
                                        <span class="author">By Author Name</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <?php include '../../component/footer.php';?>
    <?php include '../../component/scripts.php';?>
</body>

</html>