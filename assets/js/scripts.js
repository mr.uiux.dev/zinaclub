$(() => {
	// SVG
	const svgImg = $("img.svg");
	const convertSVG = function () {
		svgImg.each(function () {
			var $img = jQuery(this);
			var imgID = $img.attr("id");
			var imgClass = $img.attr("class");
			var imgURL = $img.attr("src");
			jQuery.get(
				imgURL,
				function (data) {
					var $svg = jQuery(data).find("svg");
					if (typeof imgID !== "undefined") {
						$svg = $svg.attr("id", imgID);
					}
					if (typeof imgClass !== "undefined") {
						$svg = $svg.attr("class", imgClass + " replaced-svg");
					}
					$svg = $svg.removeAttr("xmlns:a");
					$img.replaceWith($svg);
				},
				"xml"
			);
		});
	};
	convertSVG();

	// Change Navbar  on scroll
	const navbar = $(".navbar");
	$(window).scroll(function () {
		if ($(document).scrollTop() > 50) {
			navbar.addClass("on-scroll");
		} else {
			navbar.removeClass("on-scroll");
		}
	});

	// Header Slider
	const headerSlider = $(".header__slider");
	if (headerSlider.length > 0) {
		headerSlider
			.slick({
				arrows: false,
				dots: true,
				fade: true,
				autoplay: true,
			})
			.slickAnimation();
	}

	// Shop Bg Slider
	const shopBgSlider = $(".shop__bg__slider");
	if (shopBgSlider.length > 0) {
		shopBgSlider
			.slick({
				arrows: false,
				dots: true,
				fade: true,
				autoplay: true,
			})
			.slickAnimation();
	}

	//Shop Side Slider
	const shopSideSlider = $(".shop__side__slider");
	if (shopSideSlider.length > 0) {
		shopSideSlider
			.slick({
				arrows: false,
				dots: false,
				arrows: true,
				fade: false,
				autoplay: true,
			})
			.slickAnimation();
	}

	// Product Slider
	const productSlider = $(".product__slider");
	if (productSlider.length > 0) {
		productSlider.slick({
			slidesToShow: 4,
			slidesToScroll: 4,
			dots: false,
			arrows: true,
			autoplay: true,
			speed: 1500,
			responsive: [
				{
					breakpoint: 1024,
					settings: {
						slidesToShow: 3,
						slidesToScroll: 3,
					},
				},
				{
					breakpoint: 990,
					settings: {
						slidesToShow: 3,
						slidesToScroll: 3,
					},
				},
				{
					breakpoint: 767,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 2,
					},
				},

				{
					breakpoint: 450,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1,
					},
				},
			],
		});
	}
	productSlider.on("init", function (event, slick, currentSlide, nextSlide) {
		convertSVG();
	});
	const tabEl = $('button[data-bs-toggle="tab"]');
	if (tabEl.length > 0) {
		tabEl.on("shown.bs.tab", function (event) {
			event.target;
			event.relatedTarget;
			$(".product__slider").slick("setPosition");
		});
	}

	// Topic Slider
	const topicSlider = $(".topic__slider");
	if (topicSlider.length > 0) {
		topicSlider
			.slick({
				arrows: true,
				dots: false,
				autoplay: true,
			})
			.slickAnimation();
	}

	// Product Slider Details
	const productSliderFor = $(".product__slider__for");
	const productSliderNav = $(".product__slider__nav");
	if (productSliderFor.length > 0) {
		productSliderFor.slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: false,
			dots: false,
			fade: true,
			asNavFor: ".product__slider__nav",
		});
	}
	if (productSliderNav.length > 0) {
		productSliderNav.slick({
			slidesToShow: 3,
			slidesToScroll: 1,
			asNavFor: ".product__slider__for",
			dots: false,
			arrows: true,
			// centerMode: true,
			focusOnSelect: true,
		});
	}
	// Sidebar
	const theia = $(".theia");
	if (theia.length > 0) {
		theia.theiaStickySidebar({
			additionalMarginTop: 60,
		});
	}

	// Counter Product
	let minVal = 1,
		maxVal = 20; // Set Max and Min values
	// Increase product quantity on cart page
	$(".increaseQty").on("click", function () {
		var $parentElm = $(this).parents(".qtySelector");
		$(this).addClass("clicked");
		setTimeout(function () {
			$(".clicked").removeClass("clicked");
		}, 100);
		var value = $parentElm.find(".qtyValue").val();
		if (value < maxVal) {
			value++;
		}
		$parentElm.find(".qtyValue").val(value);
	});
	// Decrease product quantity on cart page
	$(".decreaseQty").on("click", function () {
		var $parentElm = $(this).parents(".qtySelector");
		$(this).addClass("clicked");
		setTimeout(function () {
			$(".clicked").removeClass("clicked");
		}, 100);
		var value = $parentElm.find(".qtyValue").val();
		if (value > 1) {
			value--;
		}
		$parentElm.find(".qtyValue").val(value);
	});
});
