<footer class="footer inner__padding pb-0">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="col-xl-9">
                        <div class="row justify-content-between">
                            <div class="col-xl col-lg-6 col-md col-12 mt-xl-0 mt-lg-2 mt-md-0 mt-2">
                                <div class="title">Company</div>
                                <div class="links">
                                    <ul class="list-unstyled mb-0">
                                        <li>
                                            <a href="">About Zina Life</a>
                                        </li>
                                        <li>
                                            <a href="">Careers</a>
                                        </li>
                                        <li>
                                            <a href="">Stores</a>
                                        </li>
                                        <li>
                                            <a href="">News</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-xl col-lg-6 col-md col-12 mt-xl-0 mt-lg-2 mt-md-0 mt-2">
                                <div class="title">Customer Services</div>
                                <div class="links">
                                    <ul class="list-unstyled mb-0">
                                        <li>
                                            <a href="">My Account</a>
                                        </li>
                                        <li>
                                            <a href="">Shipping & Returns</a>
                                        </li>
                                        <li>
                                            <a href="">FAQs</a>
                                        </li>
                                        <li>
                                            <a href="">Contact Us</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-xl col-lg-6 col-md col-12 mt-xl-0 mt-lg-2 mt-md-0 mt-2">
                                <div class="title">Explore</div>
                                <div class="links">
                                    <ul class="list-unstyled float__links mb-0">
                                        <li>
                                            <a href="">Beauty</a>
                                        </li>
                                        <li>
                                            <a href="">Wellness</a>
                                        </li>
                                        <li>
                                            <a href="">Harmony</a>
                                        </li>
                                        <li>
                                            <a href="">Food</a>
                                        </li>
                                        <li>
                                            <a href="">Travel</a>
                                        </li>
                                        <li>
                                            <a href="">Book Club</a>
                                        </li>
                                        <li>
                                            <a href="">Shop</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-xl col-lg-6 col-md col-12 mt-xl-0 mt-lg-2 mt-md-0 mt-2">
                                <div class="title">Connect With Zina</div>
                                <div class="links social">
                                    <ul class="list-unstyled mb-0">
                                        <li>
                                            <a href="" target="_blank">
                                                <i class="fab fa-facebook-f"></i>
                                                <span class="d-inline-block ms-1">Facebook</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="" target="_blank">
                                                <i class="fab fa-twitter"></i>
                                                <span class="d-inline-block ms-1">Twitter</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="" target="_blank">
                                                <i class="fab fa-youtube"></i>
                                                <span class="d-inline-block ms-1">Youtube</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="" target="_blank">
                                                <i class="fab fa-instagram"></i>
                                                <span class="d-inline-block ms-1">Instagram</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="" target="_blank">
                                                <i class="fab fa-pinterest-p"></i>
                                                <span class="d-inline-block ms-1">Pinterest</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 mt-md-0 mt-2">
                        <div class="newsletter">
                            <div class="title">Neswsletter</div>
                            <div class="desc">
                                <p>
                                    Subscribe to our newsletter and get our newest updates right on your
                                    inbox.
                                </p>
                            </div>
                            <div class="form">
                                <form action="">
                                    <input type="email" placeholder="Email Address" class="form-control" />
                                    <button type="submit" class="btn">Subscribe</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 mt-3">
                <div class="copyright">
                    <div class="logo text-end">
                        <a href="/views/home/index.html">
                            <img src="<?php echo $site_path; ?>/images/logo-footer.svg" alt="" /></a>
                    </div>
                </div>
            </div>
            <div class="col-12 my-3">
                <div class="copyright">
                    <div class="row align-items-center">
                        <div class="col-md-6 text-md-start text-center">
                            <div class="d-flex terms justify-content-md-start justify-content-center">
                                <a href="">Privacy Policy</a>
                                <a href=""> Terms Conditions</a>
                            </div>
                        </div>
                        <div class="col-md-6 mt-md-0 mt-1">
                            <div class="text-md-end text-center">
                                ©2021 <a href="">ZinaLife</a>, Inc. All Rights Reserved.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>