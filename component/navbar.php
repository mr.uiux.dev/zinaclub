<nav class="navbar navbar-expand-lg navbar-light bg-light shadow fixed-top">
    <div class="container pe-0">
        <a class="navbar-brand" href="<?php echo $site_url ?>/views/home/index.php">
            <img src="<?php echo $site_path; ?>/images/logo.png" alt="logo" />
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span></span>
            <span></span>
            <span></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mx-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link <?php if($current_page =='Home'){echo 'active';}?>"
                        href="<?php echo $site_url ?>/home/index.php">Home</a>
                </li>
                <?php 
                    $topic_list = array('Beauty', 'Wellness', 'Harmony', 'Food', 'Travel', 'Book Club');
                    foreach($topic_list as $array_values){
                        echo "<li class=\"nav-item\">
                                <a class=\"nav-link ". ($current_page == $array_values ? 'active' : ' ') ."\" href=\"" .
                                $site_url . '/topics/' . "\">
                                ". $array_values ."
                                </a>
                            </li>
                        ";
                    }
                ?>
                <li class="nav-item">
                    <a class="nav-link <?php if($current_page =='Shop'){echo 'active';}?>"
                        href="<?php echo $site_url ?>/topics/">Shop</a>
                </li>
            </ul>
        </div>
        <div class="search__cart">
            <div class="search">
                <button class="bg-transparent p-0 border-0" data-bs-toggle="modal" data-bs-target="#searchModal">
                    <img src="<?php echo $site_path; ?>/images/search.svg" class="svg" alt="cart" />
                </button>
            </div>
            <div class="cart dropdown">
                <button class="bg-transparent p-0 border-0 dropdown-toggle" type="button" id="dropdownCart"
                    data-bs-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo $site_path; ?>/images/cart.svg" class="svg" alt="cart" />
                    <div class="num">2</div>
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownCart">
                    <!-- In case of there are products -->
                    <div class="products__cart">
                        <a href="" class="product__cart d-block">
                            <div class="row">
                                <div class="col-4 pe-0">
                                    <div class="img">
                                        <img src="<?php echo $site_path; ?>/images/pro-1.jpg?w=57&h=57&scale=both&mode=crop"
                                            class="img-fluid" alt="" />
                                    </div>
                                </div>
                                <div class="col-8">
                                    <div class="title">Assertively fabricate top-line products</div>
                                    <div class="price">$120</div>
                                </div>
                            </div>
                        </a>
                        <a href="" class="product__cart d-block">
                            <div class="row">
                                <div class="col-4 pe-0">
                                    <div class="img">
                                        <img src="<?php echo $site_path; ?>/images/pro-1.jpg?w=57&h=57&scale=both&mode=crop"
                                            class="img-fluid" alt="" />
                                    </div>
                                </div>
                                <div class="col-8">
                                    <div class="title">Assertively fabricate top-line products</div>
                                    <div class="price">$120</div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="btns mt-2">
                        <a href="" class="d-block py-1 text-white text-center view__cart">View Carts</a>
                        <a href="" class="d-block py-1 text-white text-center mt-1 funds">Funds</a>
                    </div>
                    <!-- In case of empty products -->
                    <!-- <div class="empty__cart">
								<div class="mx-auto text-center mt-2">
									<img src="<?php echo $site_path; ?>/images/empty_cart.webp" class="img-fluid" alt="" />
									<div class="title h5 fw-bold text-secondary mt-1">
										No products in the cart.
									</div>
								</div>
							</div> -->
                </div>
            </div>
        </div>
    </div>
</nav>
<!-- Modal Search -->
<div class="modal fade modal__search" id="searchModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
    aria-labelledby="searchModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                <div class="row">
                    <div class="col-md-10 mx-auto">
                        <div class="form">
                            <form action="">
                                <input type="text" class="form-control" />
                                <button type="submit" class="bg-transparent p-0 border-0">
                                    <img src="<?php echo $site_path; ?>/images/search.svg" class="svg" alt="" />
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>