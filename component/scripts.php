<script src="<?php echo $site_path; ?>/lib/jquery/jquery.min.js"></script>
<script src="<?php echo $site_path; ?>/lib/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo $site_path; ?>/lib/slick/slick.min.js"></script>
<script src="<?php echo $site_path; ?>/lib/slick/slick-animation.min.js"></script>
<script src="<?php echo $site_path; ?>/lib/masonry/masonry.min.js" async></script>
<script src="<?php echo $site_path; ?>/lib/stickySidebar/ResizeSensor.js"></script>
<script src="<?php echo $site_path; ?>/lib/stickySidebar/jquery.sticky-sidebar.js"></script>
<script src="<?php echo $site_path; ?>/js/scripts.js"></script>
<script>
window.FontAwesomeConfig = {
    searchPseudoElements: true
}
</script>